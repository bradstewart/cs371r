cs371r
======

Class assignments for web search and info retrieval.

### License and Source
This code supplies "miniature" pedagogical Java implementations of
information retrieval, spidering, and other IR and text-processing
software.  It is being released for educational and research purposes only under
the GNU General Public License (see http://www.gnu.org/copyleft/gpl.html).

It was developed for an introductory course on "Intelligent Information
Retrieval and Web Search".  See:

http://www.cs.utexas.edu/users/mooney/ir-course/ 

for more information and introductory documentation (especially see the Project
assignment descriptions).

Copyleft: Raymond J. Mooney, 2001

### Modifications
I obviously modified the code for the assignments. You can do whatever you want with my additions; however, I would not recommend turning in this code if you happen to be in this class.
