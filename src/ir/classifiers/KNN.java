package ir.classifiers;

import ir.vsr.InvertedIndex;
import ir.vsr.Retrieval;

import java.util.List;

public class KNN extends Classifier {

	/**
	 * Name of classifier
	 */
	public static final String name = "K-NearestNeighbor";
	
	/**
	 * Number of categories
	 */
	int numCategories;
	
	/**
	 * Number of features
	 */
	int numFeatures;
	
	/**
	 * Number of training examples, set by train function
	 */
	int numExamples;
	
	/**
	 * Number of nearest neighbors
	 */
	int k;
	
	private InvertedIndex index;
	
	/**
	 * Flag for debug prints
	 */
	boolean debug = false;
	
	/**
	 * Sets the debug flag
	 */
	public void setDebug(boolean bool) {
	  debug = bool;
	}
	
	  
	/**
	 * Create a KNN classifier with the provided categories
	 *
	 * @param categories The array of Strings containing the category names
	 * @param debug      Flag to turn on detailed output
	 * @param k			 Num of nearest neighbors
	 */
	public KNN(String[] categories, boolean debug, int k ) {
	    this.categories = categories;
	    this.debug = debug;
	    this.k = k;
	    numCategories = categories.length;
	  }

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void train(List<Example> trainingExamples) {
			index = new InvertedIndex( trainingExamples );
			if ( debug ) { 
				System.out.println("Created InvertedIndex: \n "
										+ "  Tokens: "+index.size() + "\n"
										+ "  Documents: "+index.docRefs.size()+ "\n" 
										+ "  Categories: "+index.docRefCategories.size() ); 
			}

	}

	@Override
	public boolean test(Example testExample) {
		Retrieval[] retrievals = index.retrieve( testExample.getHashMapVector() );	
		double[] maxCat = new double[numCategories];
		
		for ( int i=0; i < retrievals.length && i < k; i++ ) {
			Retrieval r = retrievals[i];
			Integer c = index.docRefCategories.get( r.docRef );
			maxCat[c]++;
		}
		
		if ( maxCat.length < 1 ) {
			return Math.random() < 0.5;
		} else if ( testExample.getCategory() == argMax(maxCat) ) {
			return true;
		} else {
			return false;
		}
	}

}
