/**
 * 
 */
package ir.classifiers;

import ir.vsr.HashMapVector;

import java.util.ArrayList;

/**
 * Represents a category prototype vector for use by the Rocchio classifier.
 * 
 * @author Brad Stewart
 *
 */
public class RocchioPrototype {
	
	  /**
	   * A Rochio/Ide algorithm parameter
	   */
	  public static double ALPHA = 1;
	  /**
	   * A Rochio/Ide algorithm parameter
	   */
	  public static double BETA = 1;
	  /**
	   * A Rochio/Ide algorithm parameter
	   */
	  public static double GAMMA = 1;

	  /**
	   * The document vector for this query
	   */
	  public HashMapVector protoVector;
	  /**
	   * Index of this prototype in the classifier's array
	   */
	  public int category;

	  /**
	   * The list of DocumentReference's that were rated relevant
	   */
	  public ArrayList<Example> goodDocRefs = new ArrayList<Example>();
	  /**
	   * The list of DocumentReference's that were rated irrelevant
	   */
	  public ArrayList<Example> badDocRefs = new ArrayList<Example>();

	  /**
	   * Create a feedback object for this query with initial retrievals to be rated
	   */
	  public RocchioPrototype( int category ) {
		  this.category = category;
		  this.protoVector = new HashMapVector();
		  this.protoVector.clear();
	  }

	  /**
	   * Add a document to the list of those deemed relevant
	   */
	  public void addGood(Example example) {
	    goodDocRefs.add( example );
	  }

	  /**
	   * Add a document to the list of those deemed irrelevant
	   */
	  public void addBad(Example example) {
	    badDocRefs.add( example );
	  }
	

	  /**
	   * Generates a new prototype vector by adding matching documents
	   *   and removing non-matching documents.
	   *
	   * @return The revised query vector.
	   */
	  public void updatePrototype() {
	    // Normalize query by maximum token frequency and multiply by alpha
	    protoVector.addScaled( protoVector, ALPHA / protoVector.maxWeight());
	    // Add in the vector for each of the positively rated documents
	    for (Example example : goodDocRefs) {
	      // Get the document vector for this positive document
	      HashMapVector vector = example.getHashMapVector();
	      // Multiply positive docs by beta and normalize by max token frequency
	      vector.addScaled( vector, BETA / vector.maxWeight());
	      // Add it to the new query vector
	      protoVector.add(vector);
	    }
	    // Subtract the vector for each of the negatively rated documents
	    for (Example example : badDocRefs) {
	      // Get the document vector for this negative document
	      HashMapVector vector = example.getHashMapVector();
	      // Multiply negative docs by beta and normalize by max token frequency
	      vector.addScaled( vector, GAMMA / vector.maxWeight());
	      // Subtract it from the new query vector
	      protoVector.subtract(vector);
	    }
	  }
	  
	  public double cosineTo( Example example ) {
		  HashMapVector v = example.getHashMapVector();
		  return v.cosineTo(protoVector, protoVector.length());
	  }

}
