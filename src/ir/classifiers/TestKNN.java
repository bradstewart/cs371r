package ir.classifiers;

import java.util.List;

public class TestKNN {

	/**
	 * A driver method for testing the NaiveBayes classifier using
	 * 10-fold cross validation.
	 *
	 * @param args a list of command-line arguments.  Specifying "-debug"
	 *             will provide detailed output
	 */
	public static void main(String args[]) throws Exception {
		String dirName = "corpora/yahoo-science/";
		String[] categories = {"bio", "chem", "phys"};
		System.out.println("Loading Examples from " + dirName + "...");
		List<Example> examples = new DirectoryExamplesConstructor(dirName, categories).getExamples();
		System.out.println("Created "+ examples.size() +" examples.");
		System.out.println("Initializing k-Nearest Neighbor classifier...");
		
		KNN c;
		int k = 5;
		boolean debug = false;
		// setting debug flag gives very detailed output, suitable for debugging
		for ( int i=0; i < args.length; i++ ) {
			if ( args[i].equals("-debug") ) {
				debug = true;
			} else if ( args[i].equals("-K") ) {
				k = Integer.parseInt( args[++i] );
			}
		}
//		if (args.length > 0 && args[0].equals("-debug")) {
//		  debug = true;
//		} else {
//		  debug = false;
//		}
//		
//		if (args.length > 1 && args[1].equals("-K") ) {
//			
//		}
		c = new KNN(categories, debug, k);
		
		// Perform 10-fold cross validation to generate learning curve
	    CVLearningCurve cvCurve = new CVLearningCurve(c, examples);
	    cvCurve.run();
	  }


}
