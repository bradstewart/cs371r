/**
 * 
 */
package ir.vsr;

import ir.classifiers.Example;
import ir.utilities.MapUtils;
import ir.utilities.UserInput;
import ir.utilities.Weight;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * @author Brad Stewart
 * Adds bigram indexing to InvertedIndex by scanning the corpus and retaining the
 *  most common two-word phrases. The number of phrases to remember is controlled
 *  by maxPhrases.
 *
 */
public class InvertedPhraseIndex extends InvertedIndex {
	  /** Maximum number of two-word phrases to treat as tokens */
	  private static int maxPhrases = 1000;
	  /**
	   * A HashMap where bigrams are indexed. Each indexed token maps
	   * to a Weight.
	   */
	  private LinkedHashMap<String, Weight> phrases;


	/**
	 * @param dirFile
	 * @param docType
	 * @param stem
	 * @param feedback
	 */
	public InvertedPhraseIndex(File dirFile, short docType, boolean stem,
			boolean feedback, int maxPhrases) {		
	    this.dirFile = dirFile;
	    this.docType = docType;
	    this.stem = stem;
	    this.feedback = feedback;
	    tokenHash = new HashMap<String, TokenInfo>();
	    docRefs = new ArrayList<DocumentReference>();
		this.maxPhrases = maxPhrases;
		this.phrases = new LinkedHashMap<String, Weight>();
		indexDocuments();
	}

	@Override
	/**
	 *  Indexes the documents in dirFile
	 */
	protected void indexDocuments() {
		if (!tokenHash.isEmpty() || !docRefs.isEmpty()) {
		      // Currently can only index one set of documents when an index is created
	      throw new IllegalStateException("Cannot indexDocuments more than once in the same InvertedIndex");
	    }
		// Find most common phrases/bigrams
		buildPhraseHash();
	    // Get an iterator for the documents
	    DocumentIterator docIter = new DocumentIterator(dirFile, docType, stem);
	    System.out.println("Indexing documents in " + dirFile);
	    // Loop, processing each of the documents
	    while (docIter.hasMoreDocuments()) {
	      FileDocument doc = docIter.nextDocument();
	      // Create a document vector for this document
	      System.out.print(doc.file.getName() + ",");
	      // Create the bag of words including the list of phrases
	      HashMapVector vector = doc.hashMapVector( phrases );
	      indexDocument(doc, vector);
	    }

	    // Now that all documents have been processed, we can calculate the IDF weights for
	    // all tokens and the resulting lengths of all weighted document vectors.
	    computeIDFandDocumentLengths();
	    System.out.println("\nIndexed " + docRefs.size() + " documents with " + size() + " unique terms.");
  }

	/**
	 * Constructs a list of bigrams in the corpus, sorts it by frequency, and retains
	 * the amount specified by maxPhrases.
	 */
	protected void buildPhraseHash() {
		DocumentIterator docIter = new DocumentIterator(dirFile, docType, stem);
	    System.out.println("Indexing bigrams " + dirFile);
	    // Loop, processing each of the documents
	    HashMapVector phraseHashVector = new HashMapVector();
	    while (docIter.hasMoreDocuments()) {
	      FileDocument doc = docIter.nextDocument();
	      // Add the vector for all the bigrams in this document to the vector
	      //  for all bigrams in the corpus
	      phraseHashVector.add( doc.phraseHashVector() );
	    }
	    
	      // Sort the bigram vector on its values, descending
	      LinkedHashMap<String, Weight> sortedVector =  (LinkedHashMap<String, Weight>) MapUtils.sortByValueDescending(phraseHashVector.hashMap);
	      // Get a list of the sorted entries
	      ArrayList<Entry<String, Weight>> entries = new ArrayList<Map.Entry<String,Weight>>( sortedVector.entrySet() );
	      
	      // Put the top maxPhrases in the index's phrase hashMap 
	      for ( int i = 0; i < entries.size() && i < maxPhrases; i++ ) {
	    		  phrases.put( entries.get(i).getKey(), entries.get(i).getValue() );
	      }
	      System.out.println("Phrase indexing complete.");
	}


	  @Override
	  /**
	   * Enter an interactive user-query loop, accepting queries and showing the retrieved
	   * documents in ranked order. Treats bigrams in the input as single tokens if the 
	   * bigram occurs frequently in the corpus.
	   */
	  public void processQueries() {

	    System.out.println("Now able to process queries. When done, enter an empty query to exit.");
	    // Loop indefinitely answering queries
	    do {
	      // Get a query from the console
	      String query = UserInput.prompt("\nEnter query:  ");
	      // If query is empty then exit the interactive loop
	      if (query.equals(""))
	        break;
	      // Get the ranked retrievals for this query string and present them
	      HashMapVector queryVector = (new TextStringDocument(query, stem)).hashMapVector( phrases );
	      Retrieval[] retrievals = retrieve(queryVector);
	      presentRetrievals(queryVector, retrievals);
	    }
	    while (true);
	  }
	  
	  /**
	   * Index a directory of files and then interactively accept retrieval queries.
	   * Command format: "InvertedIndex [OPTION]* [DIR]" where DIR is the name of
	   * the directory whose files should be indexed, and OPTIONs can be
	   * "-html" to specify HTML files whose HTML tags should be removed.
	   * "-stem" to specify tokens should be stemmed with Porter stemmer.
	   * "-feedback" to allow relevance feedback from the user.
	   */
	  public static void main(String[] args) {
	    // Parse the arguments into a directory name and optional flag

	    String dirName = args[args.length - 1];
	    short docType = DocumentIterator.TYPE_TEXT;
	    boolean stem = false, feedback = false;
	    for (int i = 0; i < args.length - 1; i++) {
	      String flag = args[i];
	      if (flag.equals("-html"))
	        // Create HTMLFileDocuments to filter HTML tags
	        docType = DocumentIterator.TYPE_HTML;
	      else if (flag.equals("-stem"))
	        // Stem tokens with Porter stemmer
	        stem = true;
	      else if (flag.equals("-feedback"))
	        // Use relevance feedback
	        feedback = true;
	      else {
	        throw new IllegalArgumentException("Unknown flag: "+ flag);
	      }
	    }
	    
	    // Create an inverted index for the files in the given directory.
	    InvertedPhraseIndex index = new InvertedPhraseIndex(new File(dirName), docType, stem, feedback, maxPhrases);
	    for ( Map.Entry<String, Weight> entry : index.phrases.entrySet() ) {
	    	      // Print the term and its weight, where the value of the map entry is a Weight
	    	      // and then you need to get the value of the Weight as the weight.
	    	      System.out.println(entry.getKey() + ":" + entry.getValue().getValue());
	    	    
	    }
//	    index.print();
	    // Interactively process queries to this index.
	    index.processQueries();
	  }


}
