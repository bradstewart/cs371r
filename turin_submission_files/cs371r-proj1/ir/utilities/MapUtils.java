package ir.utilities;

import java.util.*;

/**
 * Static methods for sorting Maps by their values.
 * Adapted from http://stackoverflow.com/a/2581754.
 *  *
 */
public class MapUtils {
	
	/**
	 * Sorts the map by its values in descending order.
	 * @param map	
	 * @return	LinkedHashMap to ensure order
	 */
    public static <K, V extends Comparable<? super V>> Map<K, V> 
        sortByValueDescending( Map<K, V> map ) {
        List<Map.Entry<K, V>> list =
            new LinkedList<Map.Entry<K, V>>( map.entrySet() );
        Collections.sort( list, new Comparator<Map.Entry<K, V>>()
        {
            public int compare( Map.Entry<K, V> o1, Map.Entry<K, V> o2 )
            {
                return -(o1.getValue()).compareTo( o2.getValue() );
            }
        } );

        Map<K, V> result = new LinkedHashMap<K, V>();
        for (Map.Entry<K, V> entry : list)
        {
            result.put( entry.getKey(), entry.getValue() );
        }
        return result;
    }
    
    /**
	 * Sorts the map by its values in natural (ascending) order.
	 * @param map	
	 * @return	LinkedHashMap to ensure order
	 */
    public static <K, V extends Comparable<? super V>> Map<K, V> 
		    sortByValue( Map<K, V> map ) {
		    List<Map.Entry<K, V>> list =
		        new LinkedList<Map.Entry<K, V>>( map.entrySet() );
		    Collections.sort( list, new Comparator<Map.Entry<K, V>>()
		    {
		        public int compare( Map.Entry<K, V> o1, Map.Entry<K, V> o2 )
		        {
		            return -(o1.getValue()).compareTo( o2.getValue() );
		        }
		    } );
		
		    Map<K, V> result = new LinkedHashMap<K, V>();
		    for (Map.Entry<K, V> entry : list)
		    {
		        result.put( entry.getKey(), entry.getValue() );
		    }
		    return result;
    }
}