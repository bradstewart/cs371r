package ir.classifiers;

import java.util.List;

public class Rocchio extends Classifier {
	
	/**
	 * Name of classifier
	 */
	public static final String name = "Rocchio";
	
	/**
	 * Number of categories
	 */
	int numCategories;
	
	/**
	 * Flag for debug prints
	 */
	boolean debug = false;
	
	/**
	 * Flag for modifying Rocchio by subtracting un-matched document vectors
	 */
	boolean neg = false;
	
	/**
	 * Array of prototypes, one for each category
	 */
	RocchioPrototype[] prototypes;
	

	/**
	 * Create a Rocchio classifier with the provided categories
	 *
	 * @param categories The array of Strings containing the category names
	 * @param debug      Flag to turn on detailed output
	 * @param neg		 Flag to enabled negative feedback
	 */
	public Rocchio(String[] categories, boolean debug, boolean neg) {
	    this.categories = categories;
	    this.debug = debug;
	    this.neg = neg;
	    numCategories = categories.length;
	    prototypes = new RocchioPrototype[numCategories];
	  }

	@Override
	public String getName() {
		return name;
	}
	
	public void setDebug(boolean bool) {
	  debug = bool;
	}

	@Override
	public void train(List<Example> trainingExamples) {
		
		// Initialize the prototype vectors to 0
		for ( int i=0; i < prototypes.length; i++ ) {
			prototypes[i] = new RocchioPrototype(i);
		}
		
		// Add the matching vectors to the reference list
		for ( Example example : trainingExamples ) {
			RocchioPrototype p = prototypes[example.category];
			p.addGood( example );
			
			// If "-neg" parameter passed, subtract this document from other category prototypes
			if ( neg ) {
				for ( RocchioPrototype notMatched : prototypes ) {
					if ( notMatched.category != example.category ) {
						notMatched.addBad(example);
					}
				}
			}
		}
		
		// Update each prototype's vector
		for ( RocchioPrototype p : prototypes ) {
			p.updatePrototype();
		}
		
	}

	@Override
	public boolean test(Example testExample) {
		double m = -2.0;
		int c = -1;
		for ( int i=0; i < prototypes.length; i++ ) {
			RocchioPrototype prototype = prototypes[i];
			if ( prototype != null ) {
				double s = prototype.cosineTo( testExample );
				if ( s > m ) {
					m = s;
					c = i;
				}
			}
		}
		
		return c == testExample.category;
	}
	
	

}
