package ir.classifiers;

import java.util.List;

public class TestRocchio {

	/**
	 * A driver method for testing the NaiveBayes classifier using
	 * 10-fold cross validation.
	 *
	 * @param args a list of command-line arguments.  Specifying "-debug"
	 *             will provide detailed output
	 */
	public static void main(String args[]) throws Exception {
		String dirName = "/u/mooney/ir-code/corpora/yahoo-science/";
		String[] categories = {"bio", "chem", "phys"};
		System.out.println("Loading Examples from " + dirName + "...");
		List<Example> examples = new DirectoryExamplesConstructor(dirName, categories).getExamples();
		System.out.println("Initializing Rocchio classifier...");
		
		Rocchio c;
		boolean debug = false;
		boolean neg = false;
		// setting debug flag gives very detailed output, suitable for debugging
		for ( int i=0; i < args.length; i++ ) {
			if ( args[i].equals("-debug") ) {
				debug = true;
			} else if ( args[i].equals("-neg") ) {
				neg = true;
			}
		}

		c = new Rocchio(categories, debug, neg);
		
		// Perform 10-fold cross validation to generate learning curve
	    CVLearningCurve cvCurve = new CVLearningCurve(c, examples);
	    cvCurve.run();
	  }


}
