/**
 * 
 */
package ir.vsr;

import ir.classifiers.Example;
import ir.utilities.DoubleValue;
import ir.utilities.Weight;
import ir.webutils.Graph;
import ir.webutils.PageRankDirectorySpider;
import ir.webutils.PageRankSpider;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

/**
 * @author Brad
 *
 */
public class PageRankInvertedIndex extends InvertedIndex {
	
	private HashMap<String, Double> pageRanks;
	double pageRankWeight;

	/**
	 * Constructor
	 * 
	 * @param dirFile
	 * @param docType
	 * @param stem
	 * @param feedback
	 */
	public PageRankInvertedIndex(File dirFile, short docType, boolean stem,
			boolean feedback) {
		super(dirFile, docType, stem, feedback);
	}
	
	  /**
	   * Add a token occurrence to the index.
	   *
	   * @param token  The token to index.
	   * @param count  The number of times it occurs in the document.
	   * @param docRef A reference to the Document it occurs in.
	   */
	  protected void indexToken(String token, int count, DocumentReference docRef) {
	    // Find this token in the index
	    TokenInfo tokenInfo = tokenHash.get(token);
	    if (tokenInfo == null) {
	      // If this is a new token, create info for it to put in the hashtable
	      tokenInfo = new TokenInfo();
	      tokenHash.put(token, tokenInfo);
	    }
	    // Add a new occurrence for this token to its info
	    tokenInfo.occList.add(new TokenOccurrence(docRef, count));
	  }

	  /**
	   * Compute the IDF factor for every token in the index and the length
	   * of the document vector for every document referenced in the index.
	   */
	  protected void computeIDFandDocumentLengths() {
	    // Let N be the total number of documents indexed
	    double N = docRefs.size();
	    // Iterate through each of the tokens in the index
	    Iterator<Map.Entry<String, TokenInfo>> mapEntries = tokenHash.entrySet().iterator();
	    while (mapEntries.hasNext()) {
	      // Get the token and the tokenInfo for each entry in the HashMap
	      Map.Entry<String, TokenInfo> entry = mapEntries.next();

	      TokenInfo tokenInfo = entry.getValue();
	      // Get the total number of documents in which this token occurs
	      double numDocRefs = tokenInfo.occList.size();
	      // Calculate the IDF factor for this token
	      double idf = Math.log(N / numDocRefs);
	      //  System.out.println(token + " occurs in " + Math.round(numDocRefs) + " docs so IDF=" + idf);
	      if (idf == 0.0)
	        // If IDF is 0, then just remove this inconsequential token from the index
	        mapEntries.remove();
	      else {
	        tokenInfo.idf = idf;
	        // In order to compute document vector lengths,  sum the
	        // square of the weights (IDF * occurrence count) across
	        // every token occurrence for each document and store sum in docRef.length.
	        for (TokenOccurrence occ : tokenInfo.occList) {
	          occ.docRef.length = occ.docRef.length + Math.pow(idf * occ.count, 2);
	        }
	      }
	    }
	    // At this point, every document length should be the sum of the squares of
	    // its token weights.  In order to calculate final lengths, just need to
	    // set the length of every document reference to the square-root of this sum.
	    for (DocumentReference docRef : docRefs) {
	      docRef.length = Math.sqrt(docRef.length);
	    }
	  }
	
	/**
	 * Constructor with additional weight parameter for page ranking.
	 * @param dirFile
	 * @param docType
	 * @param stem
	 * @param feedback
	 * @param weight
	 */
	public PageRankInvertedIndex(File dirFile, short docType, boolean stem,
			boolean feedback, double weight) {
		super(dirFile, docType, stem, feedback);
		try {
			readPageRanks();
		} catch (NumberFormatException e) {
			System.err.println("Malformed entry in page-ranks.txt. Page ranks will be incomplete.");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Error loading page-ranks.txt.");
			e.printStackTrace();
		}
		pageRankWeight = weight;
	}



	/**
	 * Constructor
	 * 
	 * @param examples
	 */
	public PageRankInvertedIndex(List<Example> examples) {
		super(examples);
	}
	
	
	@Override
	  /**
	   * Perform PageRank ranked retrieval on this input query Document vector.
	   */
	  public Retrieval[] retrieve(HashMapVector vector) {
	    // Create a hashtable to store the retrieved documents.  Keys
	    // are docRefs and values are DoubleValues which indicate the
	    // partial score accumulated for this document so far.
	    // As each token in the query is processed, each document
	    // it indexes is added to this hashtable and its retrieval
	    // score (similarity to the query) is appropriately updated.
	    Map<DocumentReference, DoubleValue> retrievalHash =
	        new HashMap<DocumentReference, DoubleValue>();
	    // Initialize a variable to store the length of the query vector
	    double queryLength = 0.0;
	    // Iterate through each token in the query input Document
	    for (Map.Entry<String, Weight> entry : vector.entrySet()) {
	      String token = entry.getKey();
	      double count = entry.getValue().getValue();
	      // Determine the score added to the similarity of each document
	      // indexed under this token and update the length of the
	      // query vector with the square of the weight for this token.
	      queryLength = queryLength + incorporateToken(token, count, retrievalHash);
	    }
	    // Finalize the length of the query vector by taking the square-root of the
	    // final sum of squares of its token weights.
	    queryLength = Math.sqrt(queryLength);
	    // Make an array to store the final ranked Retrievals.
	    Retrieval[] retrievals = new Retrieval[retrievalHash.size()];
	    // Iterate through each of the retrieved documents stored in
	    // the final retrievalHash.
	    int retrievalCount = 0;
	    for (Map.Entry<DocumentReference, DoubleValue> entry : retrievalHash.entrySet()) {
	      DocumentReference docRef = entry.getKey();
	      double score = entry.getValue().value;
//	      score += ( pageRankWeight*pageRanks.get(docRef.toString().trim()) );
	      retrievals[retrievalCount++] = getRetrieval(queryLength, docRef, score);
	    }
	    // Sort the retrievals to produce a final ranked list using the
	    // Comparator for retrievals that produces a best to worst ordering.
	    Arrays.sort(retrievals);
	    return retrievals;
	  }
	
	@Override
	  /**
	   * Calculate the final score for a retrieval and return a Retrieval object representing
	   * the retrieval with its final score.
	   *
	   * @param queryLength The length of the query vector, incorporated into the final score
	   * @param docRef The document reference for the document concerned
	   * @param score The partially computed score 
	   * @return The retrieval object for the document described by docRef
	   *     and score under the query with length queryLength
	   */
	  protected Retrieval getRetrieval(double queryLength, DocumentReference docRef, double score) {
	    // Normalize score for the lengths of the two document vectors
	    score = score / (queryLength * docRef.length);
	    double w = (pageRankWeight*pageRanks.get(docRef.toString().trim()));
	    score += w;
	    // Add a Retrieval for this document to the result array
	    return new Retrieval(docRef, score);
	  }
	
	/**
	 * Assumes page-rank.txt exists in the directory of documents to be indexed.
	 *   Reads the file and constructs a hashmap of documents to weights.
	 *   
	 * @param fileName
	 * @throws IOException 
	 * @throws NumberFormatException 
	 */
	private void readPageRanks() throws NumberFormatException, IOException {
		pageRanks = new HashMap<String, Double>();
	
	    String line;
	    BufferedReader in = new BufferedReader(new FileReader(new File (dirFile, "page-ranks.txt")));
	    while ((line = in.readLine()) != null) {
	    	String[] doc = line.split("\\s+");
	    	if ( doc.length == 2 ) {
	    		pageRanks.put(doc[0], Double.parseDouble(doc[1]));
	    	}
	    }
	    in.close();			  
	}
	
	@Override

  /**
   * Index the documents in dirFile.
   */
  protected void indexDocuments() {
    if (!tokenHash.isEmpty() || !docRefs.isEmpty()) {
      // Currently can only index one set of documents when an index is created
      throw new IllegalStateException("Cannot indexDocuments more than once in the same InvertedIndex");
    }
    // Get an iterator for the documents
    FilenameFilter filter = new FilenameFilter() {
        public boolean accept(File directory, String fileName) {
            return !fileName.endsWith(".txt");
        }
    };
    DocumentIterator docIter = new DocumentIterator(dirFile, docType, stem, filter );
    System.out.println("Indexing documents in " + dirFile);
    // Loop, processing each of the documents

    while (docIter.hasMoreDocuments()) {
      FileDocument doc = docIter.nextDocument();
      // Create a document vector for this document
      System.out.print(doc.file.getName() + ",");
      HashMapVector vector = doc.hashMapVector();
      indexDocument(doc, vector);
    }
    // Now that all documents have been processed, we can calculate the IDF weights for
    // all tokens and the resulting lengths of all weighted document vectors.
    computeIDFandDocumentLengths();
    System.out.println("\nIndexed " + docRefs.size() + " documents with " + size() + " unique terms.");
  }
	
	/**
	   * Index a directory of files and then interactively accept retrieval queries.
	   * Command format: "InvertedIndex [OPTION]* [DIR]" where DIR is the name of
	   * the directory whose files should be indexed, and OPTIONs can be
	   * "-html" to specify HTML files whose HTML tags should be removed.
	   * "-stem" to specify tokens should be stemmed with Porter stemmer.
	   * "-feedback" to allow relevance feedback from the user.
	   */
	  public static void main(String[] args) {
	    // Parse the arguments into a directory name and optional flag

	    String dirName = args[args.length - 1];
	    short docType = DocumentIterator.TYPE_TEXT;
	    double weight = 0.0;
	    boolean stem = false, feedback = false;
	    for (int i = 0; i < args.length - 1; i++) {
	      String flag = args[i];
	      if (flag.equals("-html"))
	        // Create HTMLFileDocuments to filter HTML tags
	        docType = DocumentIterator.TYPE_HTML;
	      else if (flag.equals("-stem"))
	        // Stem tokens with Porter stemmer
	        stem = true;
	      else if (flag.equals("-feedback"))
	        // Use relevance feedback
	        feedback = true;
	      else if (flag.equals("-weight")) {
		        // Use relevance feedback
	        weight = Double.parseDouble(args[++i]);
	      } else {
	        throw new IllegalArgumentException("Unknown flag: "+ flag);
	      }
	    }

	    // Create an inverted index for the files in the given directory.
	    
	    InvertedIndex index = new PageRankInvertedIndex( new File(dirName), docType, stem, feedback, weight);
	    // index.print();
	    // Interactively process queries to this index.
	    index.processQueries();
	  }

}
