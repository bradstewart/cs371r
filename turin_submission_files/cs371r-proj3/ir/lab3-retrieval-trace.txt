java ir.vsr.PageRankInvertedIndex -html -weight 0 lab3_spider_students/
Indexing documents in lab3_spider_students
P001.html,P002.html,P003.html,P004.html,P005.html,P006.html,P007.html,P008.html,P009.html,P010.html,P011.html,P012.html,P013.html,P014.html,P015.html,P016.html,P017.html,P018.html,P019.html,P020.html,P021.html,P022.html,P023.html,P024.html,P025.html,P026.html,P027.html,P028.html,P029.html,P030.html,P031.html,P032.html,P033.html,P034.html,P035.html,P036.html,P037.html,P038.html,P039.html,P040.html,P041.html,P042.html,P043.html,P044.html,P045.html,P046.html,P047.html,P048.html,P049.html,P050.html,P051.html,P052.html,P053.html,P054.html,P055.html,P056.html,P057.html,P058.html,P059.html,P060.html,P061.html,P062.html,P063.html,P064.html,P065.html,P066.html,P067.html,P068.html,P069.html,P070.html,P071.html,P072.html,P073.html,P074.html,P075.html,P076.html,P077.html,P078.html,P079.html,P080.html,P081.html,P082.html,P083.html,P084.html,P085.html,P086.html,P087.html,P088.html,P089.html,P090.html,P091.html,P092.html,P093.html,P094.html,P095.html,P096.html,P097.html,P098.html,P099.html,P100.html,P101.html,P102.html,P103.html,P104.html,P105.html,P106.html,P107.html,P108.html,P109.html,P110.html,P111.html,P112.html,P113.html,P114.html,P115.html,P116.html,P117.html,P118.html,P119.html,P120.html,P121.html,P122.html,P123.html,P124.html,P125.html,P126.html,P127.html,P128.html,P129.html,P130.html,P131.html,P132.html,P133.html,P134.html,P135.html,P136.html,P137.html,P138.html,P139.html,P140.html,P141.html,P142.html,P143.html,P144.html,P145.html,P146.html,P147.html,P148.html,P149.html,P150.html,P151.html,P152.html,P153.html,P154.html,P155.html,P156.html,P157.html,P158.html,P159.html,P160.html,P161.html,P162.html,P163.html,P164.html,P165.html,P166.html,P167.html,P168.html,P169.html,P170.html,P171.html,P172.html,P173.html,P174.html,P175.html,P176.html,P177.html,P178.html,P179.html,P180.html,P181.html,P182.html,P183.html,P184.html,P185.html,P186.html,P187.html,P188.html,P189.html,P190.html,P191.html,P192.html,P193.html,P194.html,P195.html,P196.html,P197.html,P198.html,P199.html,P200.html,
Indexed 200 documents with 6240 unique terms.
Now able to process queries. When done, enter an empty query to exit.

Enter query:  raymond machine learning

Top 10 matching Documents from most to least relevant:

1.  P130.html            Score: 0.32986
2.  P022.html            Score: 0.16508
3.  P034.html            Score: 0.14309
4.  P068.html            Score: 0.12972
5.  P169.html            Score: 0.10081
6.  P148.html            Score: 0.09945
7.  P072.html            Score: 0.07599
8.  P142.html            Score: 0.07533
9.  P141.html            Score: 0.07278
10. P119.html            Score: 0.06595

Enter `m' to see more, a number to show the nth document, nothing to exit.

 Enter command:  

Enter query:  cryptography

Top 10 matching Documents from most to least relevant:

1.  P106.html            Score: 0.46798
2.  P023.html            Score: 0.21636
3.  P047.html            Score: 0.21231
4.  P118.html            Score: 0.21089
5.  P029.html            Score: 0.19821
6.  P070.html            Score: 0.19187
7.  P051.html            Score: 0.17569
8.  P098.html            Score: 0.02216
9.  P079.html            Score: 0.02124
10. P092.html            Score: 0.0166

Enter `m' to see more, a number to show the nth document, nothing to exit.

 Enter command:  

Enter query:  vision visualization graphics

Top 10 matching Documents from most to least relevant:

1.  P129.html            Score: 0.3249
2.  P081.html            Score: 0.30895
3.  P071.html            Score: 0.17608
4.  P094.html            Score: 0.07797
5.  P011.html            Score: 0.06632
6.  P103.html            Score: 0.06207
7.  P030.html            Score: 0.06179
8.  P035.html            Score: 0.0585
9.  P013.html            Score: 0.05729
10. P033.html            Score: 0.05694

Enter `m' to see more, a number to show the nth document, nothing to exit.

 Enter command:  

Enter query:  


java ir.vsr.PageRankInvertedIndex -html -weight 20 lab3_spider_students/
Indexing documents in lab3_spider_students
P001.html,P002.html,P003.html,P004.html,P005.html,P006.html,P007.html,P008.html,P009.html,P010.html,P011.html,P012.html,P013.html,P014.html,P015.html,P016.html,P017.html,P018.html,P019.html,P020.html,P021.html,P022.html,P023.html,P024.html,P025.html,P026.html,P027.html,P028.html,P029.html,P030.html,P031.html,P032.html,P033.html,P034.html,P035.html,P036.html,P037.html,P038.html,P039.html,P040.html,P041.html,P042.html,P043.html,P044.html,P045.html,P046.html,P047.html,P048.html,P049.html,P050.html,P051.html,P052.html,P053.html,P054.html,P055.html,P056.html,P057.html,P058.html,P059.html,P060.html,P061.html,P062.html,P063.html,P064.html,P065.html,P066.html,P067.html,P068.html,P069.html,P070.html,P071.html,P072.html,P073.html,P074.html,P075.html,P076.html,P077.html,P078.html,P079.html,P080.html,P081.html,P082.html,P083.html,P084.html,P085.html,P086.html,P087.html,P088.html,P089.html,P090.html,P091.html,P092.html,P093.html,P094.html,P095.html,P096.html,P097.html,P098.html,P099.html,P100.html,P101.html,P102.html,P103.html,P104.html,P105.html,P106.html,P107.html,P108.html,P109.html,P110.html,P111.html,P112.html,P113.html,P114.html,P115.html,P116.html,P117.html,P118.html,P119.html,P120.html,P121.html,P122.html,P123.html,P124.html,P125.html,P126.html,P127.html,P128.html,P129.html,P130.html,P131.html,P132.html,P133.html,P134.html,P135.html,P136.html,P137.html,P138.html,P139.html,P140.html,P141.html,P142.html,P143.html,P144.html,P145.html,P146.html,P147.html,P148.html,P149.html,P150.html,P151.html,P152.html,P153.html,P154.html,P155.html,P156.html,P157.html,P158.html,P159.html,P160.html,P161.html,P162.html,P163.html,P164.html,P165.html,P166.html,P167.html,P168.html,P169.html,P170.html,P171.html,P172.html,P173.html,P174.html,P175.html,P176.html,P177.html,P178.html,P179.html,P180.html,P181.html,P182.html,P183.html,P184.html,P185.html,P186.html,P187.html,P188.html,P189.html,P190.html,P191.html,P192.html,P193.html,P194.html,P195.html,P196.html,P197.html,P198.html,P199.html,P200.html,
Indexed 200 documents with 6240 unique terms.
Now able to process queries. When done, enter an empty query to exit.

Enter query:  Raymond machine learning

Top 10 matching Documents from most to least relevant:

1.  P163.html            Score: 1.56679
2.  P073.html            Score: 0.89676
3.  P103.html            Score: 0.78997
4.  P124.html            Score: 0.60404
5.  P148.html            Score: 0.54017
6.  P130.html            Score: 0.40772
7.  P072.html            Score: 0.28302
8.  P022.html            Score: 0.18749
9.  P034.html            Score: 0.16836
10. P068.html            Score: 0.15653

Enter `m' to see more, a number to show the nth document, nothing to exit.

 Enter command:  

Enter query:  cryptography

Top 10 matching Documents from most to least relevant:

1.  P079.html            Score: 2.09132
2.  P106.html            Score: 0.49879
3.  P023.html            Score: 0.2444
4.  P029.html            Score: 0.23887
5.  P047.html            Score: 0.23427
6.  P118.html            Score: 0.23196
7.  P070.html            Score: 0.23116
8.  P051.html            Score: 0.2088
9.  P098.html            Score: 0.0633
10. P092.html            Score: 0.0503

Enter `m' to see more, a number to show the nth document, nothing to exit.

 Enter command:  

Enter query:  vision visualization graphics

Top 10 matching Documents from most to least relevant:

1.  P163.html            Score: 1.5719
2.  P103.html            Score: 0.84211
3.  P129.html            Score: 0.34523
4.  P081.html            Score: 0.34215
5.  P071.html            Score: 0.19972
6.  P159.html            Score: 0.14003
7.  P094.html            Score: 0.09689
8.  P030.html            Score: 0.08484
9.  P011.html            Score: 0.0838
10. P020.html            Score: 0.08366

Enter `m' to see more, a number to show the nth document, nothing to exit.

 Enter command:  


java ir.vsr.PageRankInvertedIndex -html -weight 100 lab3_spider_students/
Indexing documents in lab3_spider_students
P001.html,P002.html,P003.html,P004.html,P005.html,P006.html,P007.html,P008.html,P009.html,P010.html,P011.html,P012.html,P013.html,P014.html,P015.html,P016.html,P017.html,P018.html,P019.html,P020.html,P021.html,P022.html,P023.html,P024.html,P025.html,P026.html,P027.html,P028.html,P029.html,P030.html,P031.html,P032.html,P033.html,P034.html,P035.html,P036.html,P037.html,P038.html,P039.html,P040.html,P041.html,P042.html,P043.html,P044.html,P045.html,P046.html,P047.html,P048.html,P049.html,P050.html,P051.html,P052.html,P053.html,P054.html,P055.html,P056.html,P057.html,P058.html,P059.html,P060.html,P061.html,P062.html,P063.html,P064.html,P065.html,P066.html,P067.html,P068.html,P069.html,P070.html,P071.html,P072.html,P073.html,P074.html,P075.html,P076.html,P077.html,P078.html,P079.html,P080.html,P081.html,P082.html,P083.html,P084.html,P085.html,P086.html,P087.html,P088.html,P089.html,P090.html,P091.html,P092.html,P093.html,P094.html,P095.html,P096.html,P097.html,P098.html,P099.html,P100.html,P101.html,P102.html,P103.html,P104.html,P105.html,P106.html,P107.html,P108.html,P109.html,P110.html,P111.html,P112.html,P113.html,P114.html,P115.html,P116.html,P117.html,P118.html,P119.html,P120.html,P121.html,P122.html,P123.html,P124.html,P125.html,P126.html,P127.html,P128.html,P129.html,P130.html,P131.html,P132.html,P133.html,P134.html,P135.html,P136.html,P137.html,P138.html,P139.html,P140.html,P141.html,P142.html,P143.html,P144.html,P145.html,P146.html,P147.html,P148.html,P149.html,P150.html,P151.html,P152.html,P153.html,P154.html,P155.html,P156.html,P157.html,P158.html,P159.html,P160.html,P161.html,P162.html,P163.html,P164.html,P165.html,P166.html,P167.html,P168.html,P169.html,P170.html,P171.html,P172.html,P173.html,P174.html,P175.html,P176.html,P177.html,P178.html,P179.html,P180.html,P181.html,P182.html,P183.html,P184.html,P185.html,P186.html,P187.html,P188.html,P189.html,P190.html,P191.html,P192.html,P193.html,P194.html,P195.html,P196.html,P197.html,P198.html,P199.html,P200.html,
Indexed 200 documents with 6240 unique terms.
Now able to process queries. When done, enter an empty query to exit.

Enter query:  raymond machine learning

Top 10 matching Documents from most to least relevant:

1.  P163.html            Score: 7.78861
2.  P073.html            Score: 4.42656
3.  P103.html            Score: 3.91013
4.  P124.html            Score: 2.92785
5.  P148.html            Score: 2.30306
6.  P072.html            Score: 1.11114
7.  P130.html            Score: 0.71914
8.  P125.html            Score: 0.43988
9.  P193.html            Score: 0.37261
10. P151.html            Score: 0.30554

Enter `m' to see more, a number to show the nth document, nothing to exit.

 Enter command:  

Enter query:  cryptography

Top 10 matching Documents from most to least relevant:

1.  P079.html            Score: 10.37162
2.  P106.html            Score: 0.62196
3.  P029.html            Score: 0.4015
4.  P070.html            Score: 0.38831
5.  P023.html            Score: 0.35657
6.  P051.html            Score: 0.34124
7.  P047.html            Score: 0.32209
8.  P118.html            Score: 0.31619
9.  P098.html            Score: 0.22782
10. P092.html            Score: 0.1851

Enter `m' to see more, a number to show the nth document, nothing to exit.

 Enter command:  

Enter query:  vision visualization graphics

Top 10 matching Documents from most to least relevant:

1.  P163.html            Score: 7.79373
2.  P103.html            Score: 3.96227
3.  P159.html            Score: 0.62077
4.  P081.html            Score: 0.47494
5.  P129.html            Score: 0.42652
6.  P152.html            Score: 0.38328
7.  P071.html            Score: 0.29427
8.  P020.html            Score: 0.1945
9.  P014.html            Score: 0.19158
10. P033.html            Score: 0.18649

Enter `m' to see more, a number to show the nth document, nothing to exit.

 Enter command:  