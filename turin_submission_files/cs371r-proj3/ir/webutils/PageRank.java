/**
 * CS 371R
 * Fall 2013
 * PageRank.java
 */
package ir.webutils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Brad Stewart
 * 
 * Simple class implementing the PageRank algorithm. Clients should only call
 *   the constructor, getPageRanks() to run the algorithm, and
 *   write() to write the ranks to file.
 *
 */
public class PageRank {
	private Double alpha = 0.15;
	private Double e;
	private HashMap<String, Double> rankings;
	private Graph graph;
	private int iterations;
	

	/**
	 * Constructor
	 */
	public PageRank(Graph graph, int iterations, double alpha) {
		this.graph = graph;
		this.iterations = iterations;
		this.alpha = alpha;
		initializeRankings();
	}
	
	/**
	 * Method for clients to run and retrieve the page rank of the graph.
	 * 
	 * @return					Hash mapping a page's URL to its rank
	 */
	public HashMap<String, Double> getPageRanks() {
		
		for ( int i=0; i < iterations; i ++ ) {
			rankPages();	
			Double c = calculateNormalizationConstant();
			normalizeRankings( c );
		}
		return rankings;
	}
	
	/**
	 * 
	 * @param file				File object with the output directory
	 * @param urlToFileName		Hash mapping the page URL to it's indexed filename
	 * @return					True if success, False if exception
	 */
	public boolean write( File file, HashMap<String, String> urlToFileName ) {
		StringBuilder content = new StringBuilder();
		StringBuilder urlRef = new StringBuilder();
		boolean result = true;
		
		
		for ( Map.Entry<String, Double> page : rankings.entrySet() ) {
			urlRef.append( urlToFileName.get( page.getKey())+".html   "+ page.getKey()+"\n");
			
			content.append( urlToFileName.get(page.getKey())+".html");
			content.append("  "+page.getValue().toString());
			content.append("\n");
		}
		
		try {
			PrintWriter out = new PrintWriter( new FileWriter( new File( file, "page-ranks.txt" ) ) );			
			out.println( content.toString() );
			out.close();
			
			PrintWriter urlRefOut = new PrintWriter( new FileWriter( new File( file, "name-to-url.txt" ) ) );
			urlRefOut.println( urlRef.toString() );
			urlRefOut.close();
			
		} catch (IOException e) {
			result = false;
			System.err.println("Failed to write PageRank file: " + e);
			e.printStackTrace();
		}
		
		return result;
	}
	
	/**
	 * Setup the initial ranking map for each node
	 */
	private void initializeRankings() {
		rankings = new HashMap<String, Double>();
		int s = graph.nodeArray().length;
		e = alpha/s;
		Double r = 1.0/s;
		for ( Node node : graph.nodeArray() ) {
			rankings.put(node.toString(), r);
		}
	}
	
	/**
	 * Calculate the page rank for the given node
	 */
	private void rankPages() {
		
		for ( Node node : graph.nodeArray() ) {			
			Double sum = 0.0;
			for ( Node pageIn : node.getEdgesIn()) {
				Double pageInRank = rankings.get( pageIn.name );
				sum += (pageInRank/pageIn.getEdgesOut().size() );
			}
			Double rank = ((1.0 - alpha)*sum + e);		
			rankings.put(node.name, rank);		
		}
	}
	
	/**
	 * Calculates the normalization constant C for the rankings.
	 * 
	 * @return		The normalization constant
	 */
	private double calculateNormalizationConstant() {
		Double sum = 0.0;
		for ( Double rank : rankings.values() ) {
			sum += rank;
		}		
		return 1/sum;
	}
	
	/**
	 * Normalizes the rankings by C = 1/|S|
	 * 
	 * @param c
	 */
	private void normalizeRankings( Double c ) {

		for ( Map.Entry<String, Double> page : rankings.entrySet() ) {
			rankings.put(page.getKey(), c*page.getValue());
		}
	}
	

}
