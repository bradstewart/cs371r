/**
 * CS 371R
 * Fall 2013
 * PageRankSpider.java
 */
package ir.webutils;

import ir.utilities.MoreMath;
import ir.utilities.MoreString;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

/**
 * @author Brad Stewart
 * 
 * Implementation of the Spider class which also uses PageRank
 *   to assign a score to each of the indexed pages.
 *
 */
public class PageRankSpider extends Spider {
	protected double ALPHA = 0.15;
	protected int ITERATIONS = 50;
	protected HashMap<String, String> urlToFileName;
	protected HashMap<String, List<Link>> potentialEdges;
	protected Graph graph;

	/**
	 * Constructor
	 */
	public PageRankSpider() {
		super();
		urlToFileName = new HashMap<String, String>();
		this.graph = new Graph();
	}
	
	
	@Override
	  /**
	   * Checks command line arguments and performs the crawl.  <p> This
	   * implementation calls <code>processArgs</code> and
	   * <code>doCrawl</code>.
	   *
	   * @param args Command line arguments.
	   */
	  public void go(String[] args) {
	    processArgs(args);
	    doCrawl();
	    doPageRank();
	  }
	
	
	/**
	 * After crawling, calculate PageRank for indexed pages.
	 */
	protected void doPageRank() {
		PageRank pr = new PageRank( graph , ITERATIONS, ALPHA );
	    pr.getPageRanks();
	    pr.write(saveDir, urlToFileName);
	}
	
	@Override
		
	  public void doCrawl() {
		HashMap<Link, List<Link>> adjMap = new HashMap<Link, List<Link>>();
	    if (linksToVisit.size() == 0 ) {
	      System.err.println("Exiting: No pages to visit.");
	      System.exit(0);
	    }
	    visited = new HashSet<Link>();
	    while (linksToVisit.size() > 0 && count < maxCount) {
	      // Pause if in slow mode
	      if (slow) {
	        synchronized (this) {
	          try {
	            wait(1000);
	          }
	          catch (InterruptedException e) {
	          }
	        }
	      }
	      // Take the top link off the queue
	      Link link = linksToVisit.remove(0);
	      System.out.println("Trying: " + link);
	      // Skip if already visited this page
	      if (!visited.add(link)) {
	        System.out.println("Already visited");
	        continue;
	      }
	      if (!linkToHTMLPage(link)) {
	        System.out.println("Not HTML Page");
	        continue;
	      }
	      HTMLPage currentPage = null;
	      // Use the page retriever to get the page
	      try {
	        currentPage = retriever.getHTMLPage(link);
	        
	      }
	      catch (PathDisallowedException e) {
	        System.out.println(e);
	        continue;
	      }
	      if (currentPage.empty()) {
	        System.out.println("No Page Found");
	        continue;
	      }
	      if (currentPage.indexAllowed()) {
	    	  adjMap.put(link, new ArrayList<Link>());
	    	  count++;
	        
	        	System.out.println("Indexing" + "(" + count + "): " + link);
	        	indexPage(currentPage);

	      }
	      if (count < maxCount) {
	        List<Link> newLinks = getNewLinks(currentPage);

	        	adjMap.put(link, newLinks);


	        linksToVisit.addAll(newLinks);
	      } 

	    }
	    for ( Entry<Link, List<Link>> source : adjMap.entrySet() ) {
	    	List<Link> destinationLinks = source.getValue();
	    	
	    	if ( destinationLinks != null && !destinationLinks.isEmpty() && destinationLinks.size() > 0 ) {
		    	for ( Link destLink : destinationLinks ) {
		    		if ( destLink != null && adjMap.containsKey(destLink) ) {
		    			graph.addEdge(source.getKey().toString(), destLink.toString());
		    		}
		    	}
	    	}
	    	
	    }

	  }
	
	/**
	 * Helper method for handling null lists in enhanced for loops
	 * 
	 * @param other
	 * @return
	 */
	public static <T> List<T> safe( List<T> other ) {
	    return other == null ? Collections.<T> emptyList() : other;
	}
	
	/**
	 * Go through the list of potential nodes and add edges for nodes
	 *   which correpsond to indexed documents.
	 */
	protected void addIndexedNodes() {
		graph.resetIterator();
		while ( graph.iterator.hasNext() ) {
			Entry<String, Node> node = graph.iterator.next();
			List<Link> links = potentialEdges.get( node.getKey() );
			if ( links != null ) {
			for ( Link link : links ) {
				if ( link != null && graph.getExistingNode( link.toString() ) != null ) {
					graph.addEdge(node.getKey(), link.toString());
				}
			}
			}
		}
	}
	
	protected void realStuff() {
		
	}
	
	protected void stuff() {
		for ( String pageUrl : potentialEdges.keySet()) {
			for ( Link link : potentialEdges.get(pageUrl) ) {
				Node nodeFrom = graph.getExistingNode(pageUrl);
				Node nodeTo = graph.getExistingNode(link.toString());
				if (  nodeFrom != null && nodeTo != null ) {
					graph.addEdge(nodeFrom.toString(), nodeTo.toString());
				}
			}
		}
	}
	
	
	@Override
	  /**
	   * "Indexes" a <code>HTMLpage</code>.  This version just writes it
	   * out to a file in the specified directory with a "P<count>.html" file name.
	   *
	   * @param page An <code>HTMLPage</code> that contains the page to
	   *             index.
	   */
	  protected void indexPage(HTMLPage page) {
		String fileName = "P" + MoreString.padWithZeros(count, (int) Math.floor(MoreMath.log(maxCount, 10)) + 1);
	    page.write(saveDir, fileName );
	    
	    urlToFileName.put(page.getLink().toString(), fileName);
	  }
	
	/**
	 * Main
	 * 
	 * @param args
	 */
	public static void main( String args[] ) {
		new PageRankSpider().go(args);
	}

}
