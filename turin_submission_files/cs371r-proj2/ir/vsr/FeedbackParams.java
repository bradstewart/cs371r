package ir.vsr;

/**
 * Simple wrapper class for feedback parameters.
 * 
 * @author Brad Stewart
 *
 */
public class FeedbackParams {
	
	/**
	 * Enumeration of Feedback Types
	 *
	 */
	public enum Type {
		USER, PSEUDO
	}
	/**
	 * The type of feedback to perform, uses the above Enum
	 */
	private Type type;
	/**
	 * Number of documents to mark relevant in pseudo-feedback
	 */
	private int numDocs = 5;
    /**
     * A Rochio/Ide algorithm parameter
     */
	private double alpha = 1.0;
	/**
	  * A Rochio/Ide algorithm parameter
	 */
	private double beta = 1.0;
	/**
	  * A Rochio/Ide algorithm parameter
	  */
	private double gamma = 1.0;
	
	/**
	 * Constructor taking only a type and number of documents for pseudo-feedback
	 * @param type
	 * @param numDocs
	 */
	public FeedbackParams( Type type, int numDocs) {
		this.type = type;
		this.numDocs = numDocs;
	}
	
	/**
	 * Constructor taking type, number of documents for pseudo-feedback, and 
	 * 	 parameters for the Rochio/Ide algorithm
	 * @param type
	 * @param numDocs
	 */
	public FeedbackParams( Type type, int numDocs, double a, double b, double g ) {
		this.type = type;
		this.numDocs = numDocs;
		this.alpha = a;
		this.beta = b;
		this.gamma = g;
	}

	/**
	 * @return	The Type of feedback
	 */
	public Type getType() {
		return type;
	}
	
	/**
	 * @param type
	 * @return	true if type is specified type
	 * 			false if not
	 */
	public boolean isType( Type type ) {
		boolean result = false;
		if ( this.type.equals( type ) ) {
			result = true;
		}
		return result;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public int getNumDocs() {
		return numDocs;
	}

	public void setNumDocs(int numDocs) {
		this.numDocs = numDocs;
	}

	public double getAlpha() {
		return alpha;
	}

	public void setAlpha(double alpha) {
		this.alpha = alpha;
	}

	public double getBeta() {
		return beta;
	}

	public void setBeta(double beta) {
		this.beta = beta;
	}

	public double getGamma() {
		return gamma;
	}

	public void setGamma(double gamma) {
		this.gamma = gamma;
	}


}
