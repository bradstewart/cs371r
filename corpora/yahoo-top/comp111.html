<base href="http://www-graphics.stanford.edu/~tolis/morph.html">
<HTML>

<HEAD>
<TITLE>3D volume morphing</TITLE>
</HEAD>

<BODY>

<CENTER>
<H1>3D volume morphing</H1>
</CENTER>

This page shows some results from the volume morphing branch of the <A
HREF="/projects/volume">volume rendering project</A> of the Stanford
computer graphics laboratory.

<P>

Notes:

<UL>

<LI> The original figures (in losslessly compressed TIFF format) can
be retrieved by clicking on the corresponding thumbnail.

<LI> All the renderings shown here, including both static images and
movies, are <EM>volume renderings</EM>, not polygon renderings.

<LI> The following MPEG movies do not capture the full quality of the
morphs.

</UL>


<HR>

<CENTER>
<H2>Human to orangutan</H2>
</CENTER>

<TABLE BORDER=8>

<TR>
<TH> Description
<TH> Source
<TH> Halfway morph
<TH> Target
</TR>

<TR>
<TD> The human head on the left is transformed into the orangutan head on
the right; the halfway morph is shown in the middle.  Both volumes
were obtained by CT scans.
<TD ALIGN="middle">
<A HREF="/papers/morph/figure5/a.tif"><IMG SRC="/papers/morph/figure5/a.gif"></A>
<TD ALIGN="middle">
<A HREF="/papers/morph/figure5/e.tif"><IMG SRC="/papers/morph/figure5/e.gif"></A>
<TD ALIGN="middle">
<A HREF="/papers/morph/figure5/c.tif"><IMG SRC="/papers/morph/figure5/c.gif"></A>
</TR>

<TR>
<TD> Movies
<TD> <A HREF="/papers/morph/video/ho-source.mpg">Human head</A>
(892KB).
<TD> <A HREF="/papers/morph/video/ho-morph.mpg">Morph sequence</A>
(570KB).
<TD> <A HREF="/papers/morph/video/ho-target.mpg">Orangutan head</A>
(836KB).
</TR>

</TABLE>


<HR>

<CENTER>
<H2>Dart to X-29</H2>
</CENTER>

<TABLE BORDER=8>

<TR>
<TH> Description
<TH> Source
<TH> Halfway morph
<TH> Target
</TR>

<TR>
<TD> The dart on the left is transformed into the X-29 on the right;
the halfway morph is shown in the middle.  Both original volumes were
obtained by scan-converting polygon meshes.
<TD ALIGN="middle">
<A HREF="/papers/morph/figure6/a.tif"><IMG SRC="/papers/morph/figure6/a.gif"></A>
<TD ALIGN="middle">
<A HREF="/papers/morph/figure6/c.tif"><IMG SRC="/papers/morph/figure6/c.gif"></A>
<TD ALIGN="middle">
<A HREF="/papers/morph/figure6/b.tif"><IMG SRC="/papers/morph/figure6/b.gif"></A>
</TR>

<TR>
<TD> Movies
<TD> <A HREF="/papers/morph/video/dx-source.mpg">Dart</A> (142KB).
<TD> <A HREF="/papers/morph/video/dx-morph.mpg">Morph sequence</A>
(69KB), and <A HREF="/papers/morph/video/dx-flyby.mpg">fly-by</A>
(3.2MB), where the dart turns into the X-29 as it flies by the viewer.
<TD> <A HREF="/papers/morph/video/dx-target.mpg">X-29</A> (268KB).
</TR>

</TABLE>


<HR>

<CENTER>
<H2>Lion to leopard-horse morph</H2>
</CENTER>

<TABLE BORDER=8>

<TR>
<TH> Description
<TH> Source
<TH> Halfway morph
<TH> Target
</TR>

<TR>
<TD> The lion on the left is transformed into the leopard-horse on the
right; the halfway morph is shown in the middle. Both original volumes
were obtained by scan-converting polygon meshes.
<TD ALIGN="middle">
<A HREF="/papers/morph/figure7/a.tif"><IMG SRC="/papers/morph/figure7/a.gif"></A>
<TD ALIGN="middle">
<A HREF="/papers/morph/figure7/c.tif"><IMG SRC="/papers/morph/figure7/c.gif"></A>
<TD ALIGN="middle">
<A HREF="/papers/morph/figure7/b.tif"><IMG SRC="/papers/morph/figure7/b.gif"></A>
</TR>

<TR>
<TD> Movies
<TD> <A HREF="/papers/morph/video/lh-source.mpg">Lion</A> (637KB).
<TD> <A HREF="/papers/morph/video/lh-morph.mpg">Morph sequence</A>
(403KB).
<TD> <A HREF="/papers/morph/video/lh-target.mpg">Leopard-horse</A>
(925KB).
</TR>

</TABLE>


<HR>

<CENTER>
<H2>Theoretical background</H2>
</CENTER>

Image morphing, the construction of an image sequence depicting a
gradual transition between two images, has been extensively
investigated. For images generated from 3D models, there is an
alternative to morphing the images themselves: <EM>3D morphing</EM>
generates intermediate 3D models, the <EM>morphs</EM>, directly from
the given models; the morphs are then rendered to produce an image
sequence depicting the transformation. 3D morphing overcomes the
following shortcomings of 2D morphing as applied to images generated
from 3D models:

<UL>

<LI> In 3D morphing, creating the morphs is independent of the viewing
and lighting parameters. Hence, we can create a morph sequence once,
and then experiment with various camera angles and lighting conditions
during rendering.  In 2D morphing, a new morph must be recomputed
every time we wish to alter our viewpoint or the illumination of the
3D model.

<LI> 2D techniques, lacking information on the model's spatial
configuration, are unable to correctly handle changes in illumination
and visibility.  Two examples of this type of artifact are: (i)
Shadows and highlights fail to match shape changes occuring in the
morph.  (ii) When a feature of the 3D object is not visible in the
original 2D image, this feature cannot be made to appear during the
morph; for example, when the singing actor needs to open her mouth
during the morph, pulling her lips apart thickens the lips instead of
revealing her teeth.

</UL>

The models subjected to 3D morphing can be described either by
geometric primitives or by volumes. Our <EM>volume</EM> morphing
operates on the latter representation for reasons described in our
upcoming SIGGRAPH '95 publication: <A HREF="/~tolis/">Apostolos
Lerios</A>, <A HREF="/~cgar/">Chase D. Garfinkle</A>, and <A
HREF="/~levoy/">Marc Levoy</A>. <A HREF="/papers/morph/">Feature-Based
Volume Metamorphosis</A>. This publication also contains a detailed
description of our algorithm.


<HR>

<CENTER>
<H2>Acknowledgments</H2>
</CENTER>

We would like to thank <A HREF="/~lacroute/">Philippe Lacroute</A> for
his help in rendering our morphs, as well as the dart to X-29 fly-by
movie.  The lion mesh was put together by <A
HREF="http://www.cs.unc.edu/~turk/">Greg Turk</A> as part of his
research on our scanner project. We used the horse mesh courtesy of
Rhythm and Hues, the color added by Greg Turk. The plastic cast of the
orangutan head was lent to us by John W. Rick and was CT scanned with
the help of Paul F. Hemler.


<HR>

<STRONG>&copy; 2003 Apostolos Lerios</STRONG>


</BODY>

</HTML>
