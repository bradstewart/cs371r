<base href="http://www.cs.utexas.edu/~lorenzo/lft.html">
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
   <meta name="GENERATOR" content="Mozilla/4.5 [en] (X11; U; SunOS 5.5.1 sun4u) [Netscape]">
   <title> Lightweight Fault-Tolerance</title>
</head>
<body text="#FFFFFF" bgcolor="#527299" link="#1EE3E5" vlink="#FFFF6A" alink="#E5924C" background="lft.html">

<center>
<h1>
Lightweight Fault-Tolerance</h1></center> <P ALIGN="Justify">As
distributed computing becomes commonplace, and many more applications
are faced with the current costs of high availability, there is a
fresh need for recovery-based techniques that combine high performance
during failure-free executions with fast recovery. However, although
the literature contains approximately 300 papers in this area,
rollback recovery is seldom used in practice to build reliable
distributed applications. The <b>Lightweight Fault-Tolerance (LiFT)
</b>project has focused on changing this state of affairs with an
approach that blends algorithmic work, systems building, and empirical
analysis.</p>

<h2> Highlights</h2>

<ul>
<P ALIGN="JUSTIFY"><li><b> <font color="#fff99">The Survey</font></b> 

This survey covers rollback-recovery techniques that do not require
special language constructs. In the first part of the survey we
classify rollback-recovery protocols into checkpoint-based and
log-based. Checkpoint-based protocols rely solely on checkpointing for
system state restoration. Checkpointing can be coordinated,
uncoordinated, or communication-induced. Log-based protocols combine
checkpointing with logging of nondeterministic events, encoded in
tuples called determinants. Depending on how determinants are logged,
log-based protocols can be pessimistic, optimistic, or
causal. Throughout the survey, we highlight the research issues
that
are at the core of rollback recovery and present the solutions that
currently address them. We also compare the performance of different
rollback-recovery protocols with respect to a series of desirable
properties and discuss the issues that arise in the practical
implementations of these protocols.</P>

<Strong>Relevant Publications:</Strong>
<ul>

<LI><STRONG> <A HREF="papers/SurveyFinal.pdf">A Survey of
Rollback-Recovery Protocols in Message-Passing Systems.</STRONG></A>
(with E. Elnozahy, Y.M. Wang and D.B. Johnson).  <EM>ACM Computing
Surveys</EM>, 34:3, September 2002, pp. 375-408.

</ul>

<P ALIGN="JUSTIFY"><li><b> <font color="#fff99">Causal Logging Protocols</font></b> 

We have developed the first formal specification of
the consistency condition common to all rollback recovery protocols,
and we have derived from it <em>causal logging</em>, a novel
technique that eliminates the traditional performance tradeoffs
between pessimistic and optimistic protocols. Causal logging protocols
perform as well as optimistic protocols during failure-free
executions, but, like pessimistic protocols, never roll back correct
processes during crash recovery. </P>
<b>Relevant Publications:</b>
<ul>

<LI><STRONG><A HREF="papers/ccjournal.pdf">Scalable Causal Message
    Logging for Wide-Area Environments.</STRONG></A> (with K. Bhatia
    and K. Marzullo).  <EM>Concurrency and Computation: Practice and
    Experience</EM>, 15:3, August 2003, pp. 873-889.

<LI><STRONG> <A HREF="papers/dc.pdf"> 
Tracking Causality in Causal Message Logging Protocols.</STRONG></A>
(with K. Bhatia and K. Marzullo). <EM> Distributed Computing</EM>,
15:1, January 2002, pp. 1-15.

<LI><STRONG> <A HREF="papers/hfbl.ps">
Scalable Causal Message Logging for Wide-Area
Environments</STRONG></A> (with K. Bhatia and K. Marzullo). In
<EM>Proceedings of the European Conference on Parallel Computing
(Euro-Par 2001)</EM>, Manchester, UK, August 2001, pp. 864-873.

<li><b><a
href="http://www.cs.utexas.edu/users/lorenzo/papers/apads.ps">
The Relative Overhead of Piggybacking in Causal Message-Logging
Protocols.</a></b>.  (with K. Bhatia and K. Marzullo) <i>Proceedings
of the Workshop on Advances in Parallel and Distributed Systems
(APADS)</i> October 20, 1998, Purdue University, West Lafayette,
Indiana.</li>

<li><b><a href="http://www.cs.utexas.edu/users/lorenzo/papers/tse.ps">
Message Logging: Pessimistic, Optimistic, Causal, and Optimal.</a></b>
(with K.  Marzullo)<i> IEEE Transactions on Software Engineering
</i>. 24:2, February 1998, pp. 149-159.</li>

<LI><STRONG> <A HREF="papers/podc.ps">
Trade-Offs in Implementing Causal Message Logging Protocols.</STRONG></A>
(with K. Marzullo) <EM> Proceedings  of the 15th ACM
Annual Symposium on the Priciples of Distributed Computing. </EM>
Philadelphia, May 1996, pp. 58-67.</LI>

<LI> <STRONG><A HREF="papers/icdcs.ps"> 
Message Logging: Pessimistic, Optimistic, and Causal.</STRONG></A>
(with K. Marzullo) <EM> Proceedings of the 15th IEEE International
Conference on Distributed Computing Systems.</EM> Vancouver, Canada,
June 1995, pp. 229-236.</li>

<LI><STRONG><A HREF="papers/readlog.ps"> 
Deriving optimal checkpoint protocols for distributed shared memory
architectures.</STRONG></A> (with K. Marzullo) <EM> Selected Papers,
International Workshop in Theory and Practice in Distributed Systems
</EM>, K. Birman, F. Mattern and A. Schiper editors, Springer-Verlag
1995, pp. 111-120.</li>

<LI> <STRONG><A HREF="papers/ftcs2.ps">
Nonblocking and Orphan-Free Message Logging
Protocols.</STRONG></A> (with B. Hoppe and K. Marzullo) <EM> Proceedings
of the 23rd International Symposium on Fault Tolerant Computing</EM>, Toulouse,
France, June 1993 pp. 145-154.</li>
 
</ul>

<P ALIGN="Justify"><li> <font color="#fff99"><b>The Egida Toolkit</b></font>

Research in rollback recovery has long suffered from
a flourishing of algorithmic results that are rarely supported by
careful experimental evaluation of their practical significance. As a
result, the performance of these algorithms in practice is not well
understood, and little attention has been given to simplifying the
difficult task of integrating rollback recovery protocols with
applications. To address these issues, we have developed the
<B>Egida</B> toolkit. Egida&#146;s design addresses for the first time
the fundamental problem of characterizing the set of functionalities
that are at the core of all message-logging protocols. This
characterization, based on a framework for handling non-determinism in
a process execution, gives Egida the expressiveness to encompass the
diversity of rollback recovery protocols. A protocol is specified
using a simple, high-level language; the protocol&#146;s
implementation is synthesized from this specification by gluing
together appropriate modules from a library. As a result, Egida allows
the implementation of arbitrary rollback recovery protocols with
minimal programming effort. A version of Egida for Solaris 2.8 is
<A href="Egida/index.html">available for downlaod</A>. </P>
<Strong>Relevant Publications:</Strong>
<ul>
<LI><STRONG><A HREF="papers/ftcs99-egida.ps"> 
Egida: An Extensible Toolkit For Low-overhead
Fault-Tolerance.</STRONG></A> (with S. Rao and H. Vin) In <EM>
Proceedings of the 29th Fault-tolerant Computing Symposium
(FTCS-29)</EM>, Madison, Wisconsin, June 1999, pp. 48-55.
</ul>

<P ALIGN="JUSTIFY"><li><font color="#fff99"><b>Understanding the Cost of Recovery</b></font>

Using Egida, we have performed the first study of the recovery
performance of message-logging protocols. This study has revealed that
no existing protocol can simultaneously guarantee low overhead during
failure-free execution, fast recovery, and fault-containment, leaving
applications to face a complex tradeoff. To eliminate this tradeoff,
we have developed a new class of protocols that never roll back
correct processes, recover quickly (within 2% of the protocol with the
best recovery time) and impose little overhead during failure-free
execution (within 2% of the protocol with the best failure-free time).

<Strong>Relevant Publications:</Strong>
<ul>

<LI> <STRONG> <A HREF= "papers/tkde2000.pdf"> 
The Cost of Recovery in Message Logging Protocols.</STRONG></A> (with
S. Rao and H.Vin). <EM> IEEE Transactions on Knowledge and Data
Engineering</EM>, 12:2, March/April 2000, pp. 160-173.


<li><b>The Cost of Recovery in Message Logging Protocols.</b> (with S. Rao
and H.Vin). <i>Proceedings of the 17th International Symposium on
Reliable Distributed Systems.</i> West Lafayette, Indiana, October
1998.</li>


<li><b><a href="http://www.cs.utexas.edu/users/lorenzo/papers/ftcs98.ps">
Hybrid Message-Logging Protocols for Fast Recovery.</a></b> (with
S. Rao and H.  Vin) <i>Digest of FastAbstracts. The 28th International
Symposium on Fault-Tolerant Computing.</i> Munich, Germany, June 1998,
pp. 41-42.</li>

</ul>
<P ALIGN="JUSTIFY"><li><font color="#fff99"> <b>An Analysis of
Communication-Induced Checkpointing</b> </font>

Communication-induced checkpointing considerable autonomy in deciding
when to take checkpoints and guarantee that no process will ever take
a <I>useless</I> checkpoint, i.e. a checkpoint that will never belong
to any consistent global state.  Before our study, it had been
qualitatively argued that the value of CIC protocols lies in their
ability to allow processes to take checkpoints independently.  We have
shown that this ability offers no substantial benefit.  First, even
though choosing when to take a checkpoint can be useful --- for
example, a process can delay checkpointing until it has very little
state to record --- our experiments show that the number of
<I>forced</I> checkpoints CIC imposes to ensure no checkpoints are
useless are more than twice the number of independent
checkpoints. Second, the number of forced checkpoints grows linearly
with the number of processes in the system, suggesting that CIC
protocols may be difficult to scale. Third, the <I>nature</I> of a
checkpoint in CIC hurts performance.  While in coordinated
checkpointing processes can checkpoint asynchronously, in CIC
processes must checkpoint synchronously to prevent the creation of
useless checkpoints. As a result, the overhead imposed by CIC during
failure-free executions is about an order of magnitude higher than
that of coordinated checkpointing.</P> <Strong>Relevant
Publications:</Strong> <ul> <LI> <STRONG><A
HREF="papers/ftcs99-cic.ps"> An Analysis of Communication-Induced
Checkpointing.</STRONG></A> (with E. Elnozahy, S. Rao, S.A. Husain and
A. de Mel) In <EM> Proceedings of the 29th Fault-tolerant Computing
Symposium (FTCS-29)</EM>, Madison, Wisconsin, June 1999, pp. 242-249.
</ul>

<P ALIGN="JUSTIFY"><li><font color="#fff99"><b>Efficient support of file
I/O</b></font> 

Traditional rollback recovery techniques treat the file system as part
of the "outside world". As a result, processes may be forced to
execute a blocking <i>output commit</i> protocol whenever they
interact with the file system.  <br>We have derived a new protocol
that integrates records and efficiently replicates the information
necessary to reproduce file I/O operations during recovery. Our
simulation studies show that this approach eliminates all synchronous
logging to stable storage, thereby reducing the cost of performing
file I/O dramatically.</P>

<STRONG>Relevant Pulications: </Strong>
<ul>

<li><b><a href="http://www.cs.utexas.edu/users/lorenzo/papers/icdcs98.1.ps">
Low-Overhead Protocols for Fault-Tolerant File Sharing.</a></b> (with
S. Rao and H.  Vin) <i>Proceedings of the 18th IEEE International
Conference on Distributed Computing Systems.</i> Amsterdam, The
Netherlands, May 1998, pp. 452-461.</li>

</ul>

<P ALIGN="JUSTIFY"><li><font color="#fff99"><b> Fault-Tolerant TCP.</font> </B>

We have developed an implementation of a fault-tolerant TCP (FT-TCP)
that allows a faulty server to keep its TCP connections open until it
either recovers or it is failed over to a backup.  The failure and
recovery of the server process are completely transparent to client
processes connected with it via TCP.  FT-TCP does not affect the
software running on a client, does not require to change the server's
TCP implementation, and does not use a proxy.  We have studied the
performance of FT-TCP for Microsoft's Samba file server and a
multimedia streaming server from Apple comparing two implementations
of FT-TCP, one based on primary-backup and another based on message
logging. Our focus was on scalability, failover time, and application
transparency. Our experiments suggest that FT-TCP is a practicable
approach for replicating TCP/IP-based services that incurs low
overhead on throughput, scales well as the number of clients
increases, and allows recovery of the service in near-optimal time.
<P>
Although the details of our solution are specific to TCP, the 
architecture that we propose is sufficiently general to be 
applicable in principle to other connection-oriented network 
protocols.</P></li>

<Strong>Relevant Publications:</Strong>
<ul>
<LI><STRONG><A HREF = "papers/fttcp2.pdf"> 
Engineering Fault Tolerant TCP/IP Services Using FT-TCP</A></STRONG>
(with D. Zagorodnov, K. Marzullo, and T. Bressoud). To appear in
<EM> Proceedings of the International Conference on Dependable
Systems and Networks (DSN 2003), DCC Symposium</EM>, June 2003.

<LI> <STRONG> <A HREF= "papers/fttcp.ps">
Wrapping Server-Side TCP to Mask Connection Failures.</STRONG></A>
(with T.C. Bressoud, A. El-Khashab, K. Marzullo, and D. Zagorodnov).
In <em> Proceedings of Infocom 2001 </em>, Anchorage, Alaska, April
2001, pp. 329-338.

</ul>

<P ALIGN="JUSTIFY"><li><font color="#fff99"><b>A Fault-Tolerant JVM. </B></font>We have showed,
for the first time, how to apply state machine replication to
multi-threaded state machines. In particular, we have modified the Sun
JDK1.2 to provide transparent fault tolerance for many Java
applications using a primary backup architecture. We identify the
sources of nondeterminism in the JVM (including asynchronous
exceptions, multi-threaded access to shared data, and the
nondeterminism present at the native method interface) and guarantee
that primary and backup handle them identically.  We analyze the
overhead introduced in our system by each of these sources of
non-determinism and compare the performance of different techniques
for handling multithreading.</P></li>

<STRONG>Relevant Publications:</STRONG>

<ul>

<LI><STRONG><A HREF= "papers/ftjvm.pdf"> A Fault-Tolerant Java Virtual
Machine</A></STRONG> (with J. Napper and H. Vin). To appear in <EM>
Proceedings of the International Conference on Dependable Systems and
Networks (DSN 2003), DCC Symposium</EM>, June 2003.

<li>
<b><a href="http://www.cs.utexas.edu/users/lorenzo/papers/hcw98.ps">Fault-Tolerance:
Java's Missing Buzzword.</a></b> Invited Paper. <i>Proceedings of the 7th
Heterogeneous Computing Workshop (HCW '98)</i>, Orlando, Florida, March
1998, pp. 156-158.</li>

</ul>

</ul>

<hr>

<h2> Collaborators and Alumni</h2>

<ul>
<li><a href="http://personal.denison.edu/~bressoud">Tom Bressoud
(Denison University)</a> </li> 
<li> Elmootazbellah Elnozahy (IBM Austin Research Lab) </li>
<li> <a href="http://www.cs.ucsd.edu/~marzullo/">Keith Marzullo
(UCSD) </a></li>
<li><a href="http://www.cs.utexas.edu/users/jmn">Jeff Napper</a></li>
<li> Sriram Rao</a>, Ph.D. August 1999, UT Austin</li>
<li><a href="http://www.cs.utexas.edu/users/vin">Harrick Vin</a></li>
<li>Kalpana Ravinarayanan</a>, M.S. May 2000, UT Austin</li>
<li> <a href="http://grid-devel.sdsc.edu/karan/karan.html">Karan
Bhatia</a>, Ph.D. December 2000, UCSD </li> 
<li> Ravishankar
C.V.</a>, M.S. May 2001, UT Austin</li><li> Dmitri Zagarodnov (UCSD) </li>
</ul>


<h2>Sponsors</h2>

The LiFT project was supported by several governmental and private
sponsors:
<br>
<ul>
<LI> The <a href="http://www.sloan.org">Alfred P. Sloan Foundation</A>
through  a Research Fellowship</LI>

<LI><a href="http://www.nsf.gov">NSF</A> through  a CAREER award, Grant
no. 9734185</LI>

<li><a href="http://www.darpa.mil/ito">DARPA</a> through its support
of the <A HREF="http://www-cse.ucsd.edu/users/marzullo/WAFT/index.html">WAFT project</a></li>

<LI><A HREF="http://www.dell.com">Dell</A> through a <A
HREF="http://www.utexas.edu/computer/grants/lariat/index.htm">LARIAT
award</A></LI>

<LI><A HREF="http://www.ibm.com">IBM</A> through a Faculty
Partnership award</A></LI>
</ul></p>



<A HREF="http://www.cs.utexas.edu/users/lorenzo/">Back</A> to Lorenzo
Alvisi's Home page.
</body>
</html>
