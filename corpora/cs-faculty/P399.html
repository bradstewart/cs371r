<base href="http://www.cs.utexas.edu/~moore/atp/">
<HTML>
<BODY>
<HEAD>
<TITLE>
Mechanized Theorem Proving Research Group
</TITLE>
</HEAD>

<H1>Mechanized Theorem Proving Research Group</H1>

<TABLE BORDER=0 CELLSPACING=10 CELLPADDING=5 WIDTH=100%>
<TR>
<TD>
<CENTER>
<IMG SRC="bledsoe.gif">
</CENTER>
</TD>
<TD>
<EM>``When you have something that looks like a good idea, give it your best
shot''</EM><BR> -- <A HREF="http://www.cs.utexas.edu/users/boyer/bledsoe-bio.pdf">Woody Bledsoe, 1921--1995</A>
<P>
<EM>``The reason so many deep theorems are proved with the Boyer-Moore theorem
prover is that only smart people use it!''</EM><BR> -- anonymous critic
</TD>
</TR>
</TABLE>

Can we build machines that reason?  Yes!  UT Austin has a long tradition of
building such machines, going back to the pioneering work of Woody Bledsoe
(photo above) from the 1960's onwards.
<P>
Our research group is the home of some of the most
powerful automatic theorem proving engines in the world, including <A
HREF="http://www.cs.utexas.edu/users/moore/acl2/index.html">ACL2</A> and its
predecessor, the Boyer-Moore theorem prover <A
HREF="ftp://ftp.cs.utexas.edu/pub/boyer/nqthm/index.html">Nqthm</A>.
The authors of this software (Bob Boyer, J Moore, and
Matt Kaufmann; see below) won the <A HREF="http://awards.acm.org/software_system/">2005 ACM Software System Award</A>,
awarded for software systems
that have had lasting influence, ``reflected in contributions to concepts, in commercial acceptance, or both.''
<P>
The faculty members in automatic theorem proving research at UT are Bob Boyer (now retired but Emeritus),
Warren A. Hunt, Jr.,
and J Moore.  As a co-author of ACL2 and a long-time contributor to the Boyer-Moore
project, Matt Kaufmann is a research staff member who is also an active member of the group.
For more information see our home pages (<A
HREF="http://www.cs.utexas.edu/users/boyer/index.html">Boyer</A>, <A
HREF="http://www.cs.utexas.edu/users/hunt/">Hunt</A>, <A
HREF="http://www.cs.utexas.edu/users/moore/welcome.html">Moore</A>, <A
HREF="http://www.cs.utexas.edu/users/kaufmann/index.html">Kaufmann</A>), or
contact one of us by email (last name cs utexas edu, with an at-sign and dots in the
obvious places).
<P>
We meet regularly to discuss current issues in automatic theorem proving.
These days our meetings largely concern ongoing ACL2 projects or connections
between ACL2 and other theorem proving methodologies.  Our seminar schedule is <a
href="http://www.cs.utexas.edu/users/moore/acl2/seminar/">posted
online</a> (including past meetings) and those interested in automated
reasoning are welcome to attend.
<P>
For a little more information about what we do, read on.  
<A
HREF="http://www.cs.utexas.edu/users/moore/publications/acl2-books/index.html">Two
books about ACL2</A> describe our work in more detail.  See especially
<A HREF="http://www.cs.utexas.edu/users/moore/publications/acl2-books/car-overview.ps">Overview (.8 MB postscript file)</A> of the first book.

<HR>
<H1>What Have We Proved?</H1>

Among the many theorems proved with UT theorem provers are the following:
<UL>
<LI> the limit of a sum is the sum of the limits,
<LI> the Bolzano-Weierstrass Theorem,
<LI> the Fundamental Theorem of calculus,
<LI> Euler's identity,
<LI> Gauss' law of quadratic reciprocity, 
<LI> the undecidability of the halting problem, and
<LI> Godel's incompleteness theorem.
</UL>
Clearly, our theorem provers touch upon and often raise deep and interesting
questions in mathematics, logic and artificial intelligence.
<P>
But our theorem provers are not just of academic interest.  A specialty of
our theorem provers is the correctness of ``digital artifacts,'' i.e.,
hardware designs, instruction set architectures, microarchitecture models,
and software.  UT theorem provers have been used to prove the correctness of

<UL>
<LI> the gate-level design of an academic microprocessor --- which was
then fabricated and tested,

<LI> a compiler, an assembler, a linker, and a loader for the above
      microprocessor,

<LI> application programs written in the source language of the above
      compiler

<LI> the ``CLI stack'' -- the chaining together of the above theorems to
      establish correctness of applications running on a fabricated chip,

<LI> 21 of the 22 routines in the Berkeley C String library (when compiled
      by <CODE>gcc -o</CODE> for the Motorola 68020),

<LI> microcode programs extracted from the ROM of the Motorola CAP digital
     signal processor,

<LI> the microcode for floating-point division and square root on the AMD K5,

<LI> the RTL implementing each of the elementary floating-point operations
      on the AMD Athlon,

<LI> safety-critical code involved in trainborne control software written
      by Union Switch and Signal,

<LI> components of the Rockwell-Collins Avionics JEM1, the world's first
      Java virtual machine in silicon,

<LI> bootstrapping code for the IBM 4758 secure co-processor,

<LI> class loader and bytecode verifier for the Sun Java Virtual Machine

<LI> process separation on the Rockwell-Collins AAMP7G cryptoprocessor, allowing
the processor to obtain Multiple Independent Levels of Security (MILS) certification using
formal methods techniques as specified by EAL-7 of the Common Criteria

</UL>

Many of the proofs above were undertaken by researchers working for the
companies named.  The proofs often uncovered bugs in well-tested designs --
bugs that were fixed before fabrication.  
<P>
Thus, our theorem provers are valuable industrial tools.  Mechanical theorem
proving may be industry's best hope for coping with the complexity of modern
designs -- and industry knows it!  The industry today seeks people with the
skills to use and develop these tools.
<P>
<HR>
<H1>A Thumbnail History</H1>

Bledsoe is largely responsible for establishing a theorem proving tradition
at UT.  But others have had important roles, perhaps none more so than Don
Good, who with J.C. Browne and Bledsoe established and led the Institute for
Computing Science and Computer Applications (ICSCA).  ICSCA was located in
the 20th and 21st floors of the UT Tower.  In the 1970's and early 80's, the
major research thrust of ICSCA was program verification.  Good and his
colleagues developed one of the earliest successful program verification
systems, named Gypsy.  The theorem prover in the Gypsy verification system
was initially based on the prover developed by Bledsoe in the mid-70's. 
While the Gypsy prover evolved a lot, its relationship to the Bledsoe
provers was always clear.
<P>
In 1981, Boyer and Moore joined the CS faculty at UT and also joined ICSCA.
Their Nqthm theorem prover became a second main thrust at ICSCA.
<P>
In 1983, Good, Rich Cohen, Boyer, Moore, and Mike Smith founded <A
HREF="http://www.cli.com">Computational Logic, Inc.</A> (CLI).  The goal of
the corporation was to move verification technology to industry and to
continue formal methods research.  For several years CLI existed only on
paper; the principals performed private consulting and donated part of the
revenue to the company.  In 1987, CLI acquired offices off campus and within
a few years the entire ICSCA operation had moved to CLI, under the leadership
of Don Good, who was both President and Chairman of the Board.  The company,
still oriented toward transfer of verification technology to industry, did
not sell a product but performed contract research for government and
industry.  Ownership in the company was distributed among the employees.
Boyer remained a UT professor; Moore was an adjunct professor and taught
several courses while at CLI, including one (on machine-code verification) on
the CLI premises.  Many PhDs were awarded to students who were working at
CLI.
<P>
In 1999, CLI officially closed its doors and distributed its assets to the
shareholders.  Many ex-CLI employees are now working in formal methods in
industry.
<P>
Many of the fundamental advances in theorem proving made by the UT Mechanized
Theorem Proving research group actually occurred as contract research
conducted at CLI.  (For example, the Motorola CAP DSP verification and the
AMD K5 floating-point division proof were CLI projects.)
<P>
In addition to the theorem provers of Bledsoe, Boyer, Moore and Kaufmann, a
number of other systems have been created by members of our group, including
a remarkable geometry theorem prover by Shang-ching Chou,
<A HREF="http://pvs.csl.sri.com">PVS</A> by Natarajan Shankar and others, 
the <A HREF="http://www.dcs.ed.ac.uk/home/lego/">Lego</A> system for
constructive type theory by
Randy Pollack, and a variant of ACL2 supporting non-standard analysis
by Ruben Gamboa (see the documentation topic <CODE>real</CODE> in the
<A HREF="http://www.cs.utexas.edu/users/moore/acl2">ACL2 User's Manual</A>).

<HR>
<H1>People</H1>

Here are some of the people who have been in this research group in some
capacity.  This list includes several people who have made fundamental
contributions to theorem proving and formal methods.  Some members were
students here.  Some students completed Masters or PhDs at UT; others went
elsewhere and sometimes completed their degrees there.  Some members were
post-docs, long-term visitors, research scientists or engineers in our group at
the University of Texas at Austin, or employees of Computational Logic, Inc.  The
list is offered as evidence of the long life and activity of the group, not
merely as a list of graduated students.  The list is in roughly reverse
chronological order.  It does not include all of the professionals using ACL2
in industry, some of whom have made deeply significant contributions to the
tool and the ACL2 community.

<UL>
<LI>Shilpi Goel
<LI>Oswaldo Olivo
<LI>Alan Dunn
<LI>Nathan Wetzler
<LI>Jared Davis
<LI>Sol Swords
<LI>John Erickson
<LI>Ian Wehrman
<LI>David Rager
<LI>Qiang Zhang
<LI>Sandip Ray
<LI>Carlos Pacheco
<LI>Michael N. Bogomolny
<LI>Robert Krug
<LI>Rob Sumners
<LI>Pete Manolios
<LI>Jun Sawada
<LI>Ruben Antonio Gamboa
<LI>Benjamin Price Shults
<LI>Ken Albin
<LI>John Cowles
<LI>Matthew Michael Wilding
<LI>Sakthikumar Subramanian
<LI>Nicholas Freitag McPhee
<LI>Yuan Yu
<LI>Arthur David Flatau
<LI>Jimi Crawford
<LI>David Moshe Goldschlag
<LI>Randy Pollack 
<LI>James Daniel Christian
<LI>William R. Bevier
<LI>William D. Young
<LI>Ann Siebert
<LI>Judy Crow
<LI>Anand Tripathi
<LI>Ben DiVito
<LI>John McHugh
<LI>Guohui Feng
<LI>David Plummer
<LI>Don Simon
<LI>Larry Hines
<LI>Miren Carranza
<LI>Tie Cheng Wang
<LI>Myung Won Kim
<LI>Michael Vose
<LI>Matt Kaufmann
<LI>David Russinoff
<LI>Natarajan Shankar
<LI>Warren Alva Hunt, Jr.
<LI>Shang-Ching Chou
<LI>Mabry Tyson
<LI>Dwight Hare
<LI>Wilhelm Burger
<LI>Charlie Hoch
<LI>Larry Hunter
<LI>Jim Williams
<LI>Robert L. (Larry) Akers
<LI>Larry Smith
<LI>Mike Smith
<LI>Rich Cohen
<LI>John T Minor
<LI>Peter Bruell
<LI>Mark Steven Moriconi
<LI>Vesko Genov Marinov
<LI>J Strother Moore
<LI>Donald I. Good
<LI>Dallas Lankford
<LI>Mike Ballantyne
<LI>Robert S. Boyer
<LI>Robert Anderson
<LI>Charles Wilks
<LI>James Morris
<LI>Stephen Charles Darden
<LI>Morris C Enfield
<LI>John Ulrich
<LI>James Hall
<LI>Claude Duplissey
<LI>Hugh Williamson
</UL>
</BODY>
</HTML>
