<base href="http://www.cs.utexas.edu/~mooney/ir-course/proj4/">
<html>

<head>
<title>CS 371R Information Retrieval and Web Search: Project 4</title>
</head>

<body>
<center>
<h2>Project 4 for CS 371R: <br>
Text Categorization using Naive Bayes, KNN and Rocchio</h2>
Due date: December 3, 2013
</center>
<br>
<h3> Existing Categorization Framework</h3>

As discussed in class, a basic text categorization framework is available in
<code>ir.classifiers</code>. See the <a
href="../doc/ir/classifiers/package-summary.html">Javadoc</a> for this
code. Right now, <code>ir.classifiers</code> has the <a
href="../doc/ir/classifiers/NaiveBayes.html"><code>NaiveBayes</code></a>
classifier in it. The <code>NaiveBayes</code> class is created by extending the
abstract class <a
href="../doc/ir/classifiers/Classifier.html"><code>Classifier</code></a>. The
<code>NaiveBayes</code> classifier performs text categorization using the Naive
Bayes method, and does Laplace smoothing. It also stores all probabilities
internally in log values, to prevent underflow problems.  The
<code>NaiveBayes</code> class has a <code>train</code> method that takes in a
vector of training documents as classified <a
href="../doc/ir/classifiers/Example.html"><code>Example</code></a> objects.
The ir.classifiers package also has a <a
href="../doc/ir/classifiers/BayesResult.html"><code>BayesResult</code></a>
class, which holds the result of training a Naive Bayes classifier.

The code is currently set up to categorize the
<i>yahoo-science</i> document collection at
<code>/u/mooney/ir-code/corpora/yahoo-science/</code> into 3 categories -
<i>bio</i>, <i>phys</i>, <i>chem</i>. It
also has a <code>test</code> method that takes in a test <code>Example</code>
and categorizes it as a <i>bio</i>, <i>phys</i> or <i>chem</i> document.


<p> The <a href="../doc/ir/classifiers/CVLearningCurve.html"><code>CVLearningCurve</code></a>
class generates the learning curves with <i>k</i>-fold (default <i>k</i>=10) cross validation for a
classifier. The <a
href="../doc/ir/classifiers/TestNaiveBayes.html"><code>TestNaiveBayes</code></a> class creates a
<code>NaiveBayes</code> classifier object and runs 10-fold cross-validation of the classifier on
the <i>yahoo-science</i> dataset.  The output of <code>CVLearningCurve</code> are two <code>.gplot</code> files, one
for classification accuracy on the training examples, and another for accuracy on the
test data.  <i>Gnuplot</i> can use these files to generate the learning curves
(plots) in Postscript. To run <i>gnuplot</i> on a file, just execute "<code>gnuplot filename.gplot 
&gt; filename.ps</code>".  You can view the <code>.ps</code> file with "<code>gv filename.ps</code>" (Ghostview).  See a
<a href="sample-trace.txt">sample trace</a> of running <code>TestNaiveBayes</code> on the
<i>yahoo-science</i> document collection (using the command "<code>java
ir.classifiers.TestNaiveBayes</code>"), and the learning curves for
<a href = "NaiveBayes.gplot.pdf">testing</a> and <a href="NaiveBayesTrain.gplot.pdf">training</a>
accuracy results produced using <i>gnuplot</i>.

<h3>Your Task</h3>

<p> Your assignment is to create KNN and Rocchio classifiers by extending the
<code>Classifier</code> abstract class. The <code>KNN</code> class should
perform the simple <i>K</i>-nearest neighbor categorization algorithm, as
discussed in class.  [<b>Hint:</b> Using the <a
href="../doc/ir/vsr/InvertedIndex.html#InvertedIndex(java.util.List)"><code>InvertedIndex(List
examples)</code></a> constructor from <a
href="../doc/ir/vsr/InvertedIndex.html"><code>InvertedIndex</code></a> in
<code>ir.vsr</code> would make it easier to implement KNN.]  

<p>
The <code>Rocchio</code> class should perform Rocchio categorization.
[<b>Hint:</b> to prevent longer documents from having more influence, you still
want to normalize document vectors by the maximum weight of a token before
adding (or subtracting) them to create a prototype.  Since <a
href="../doc/ir/vsr/HashMapVector.html#multiply(double)">
<code>HashMapVector.multiply(double)</code></a> is destructive, and document
vectors are reused in <code>Example</code>s, you may use <a
href="../doc/ir/vsr/HashMapVector.html#addScaled(ir.vsr.HashMapVector, 
double)"> <code>HashMapVector.addScaled(HashMapVector, double)</code></a> to
avoid the need to create a scaled copy when constructing prototypes.]

<p>Besides the normal version of Rocchio, where the prototype vectors in each
category are generated by adding the documents in that category, you should
implement a modified version of Rocchio in which the prototype vectors in each
category are generated by adding the documents in that category as well as <i>
subtracting</i> the documents in all other categories.

<p>You need to create <code>TestKNN</code> and <code>TestRocchio</code> classes
to run 10-fold cross validation of the corresponding algorithms.
<code>TestRocchio</code> should take a command-line parameter <code>-neg</code>
that would invoke the modified version of Rocchio, while <code>TestKNN</code>
should take a command-line parameter <code>-K</code> which would then invoke
KNN with the value of <i>K</i> following <code>-K</code>.  If the
<code>-K</code> option is not present, categorization should be performed using
the default <i>K</i> value of 5.  You will use these two classes like
<code>TestNaiveBayes</code> to generate learning curves for the two classifiers
that you will create.  Manually combine the learning curves of
Naive Bayes, normal Rocchio, modified Rocchio and KNN for <i>K</i> values of 1,
3 and 5 into two <i>gnuplot</i> files, one for the training data and one for
the testing data.  Each of the final plots should have 6 learning curves - put
these in files called <code>final-results-train.ps</code> and
<code>final-results-test.ps</code>.

<p>To recap, the command-line syntax for <code>TestKNN</code> and <code>TestRocchio</code> should be:

<pre>
    java ir.classifiers.TestKNN [-K <i>K</i>]
    java ir.classifiers.TestRocchio [-neg]
</pre>

<p>In the writeup, include detailed discussions of:

<ol>
<li> Comparative accuracy of the algorithms at different points on the learning curve for the training data.
<li> Comparative accuracy of the algorithms at different points on the learning curve for the testing data.
<li> Comparative running times of the algorithms in training and testing phases.  Include a table of training and testing times for each algorithm, as reported by <code>CVLearningCurve</code>. 
</ol>

<p>A significant portion of the total credit will be based on the writeup. Try
to give a good analysis and interpretation of the results in the writeup.  
Include a good description of the algorithms you implemented and what you observed about their
behaviors.

<h3>Submission</h3>
<p>
In submitting your solution, follow the general course instructions on
<a href="../proj-submit-info.html">submitting projects</a> on the course homepage.
<p>
Along with that, follow these specific instructions for Project 4:

<ul>

<li> For this project, submit a directory called <code>proj4</code>, which should
contain:

<ol>
<li> <code>REPORT.*</code>:      detailed writeup in text or html or ps or pdf
<li> <code>soln-trace</code>:          trace file of program execution (for all 6 runs) on yahoo-science
<li> <code>final-results-train.ps</code>:    final Postscript <i>gnuplot</i> output for the training data (6 learning curves) for yahoo-science
<li> <code>final-results-test.ps</code>:    final Postscript <i>gnuplot</i> output for the testing data (6 learning curves) for yahoo-science
<li> <code>Java files</code>:  Any Java classes you modify or add.
</ol>
</ul>

</body>
</html>

