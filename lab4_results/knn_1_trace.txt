java ir.classifiers.testKNN -K 1

Loading Examples from corpora/yahoo-science/...
java.lang.ArrayIndexOutOfBoundsException: 88
	at javax.swing.text.html.parser.ContentModel.first(Unknown Source)
	at javax.swing.text.html.parser.ContentModel.first(Unknown Source)
	at javax.swing.text.html.parser.ContentModelState.advance(Unknown Source)
	at javax.swing.text.html.parser.TagStack.advance(Unknown Source)
	at javax.swing.text.html.parser.Parser.legalElementContext(Unknown Source)
	at javax.swing.text.html.parser.Parser.legalTagContext(Unknown Source)
	at javax.swing.text.html.parser.Parser.parseTag(Unknown Source)
	at javax.swing.text.html.parser.Parser.parseContent(Unknown Source)
	at javax.swing.text.html.parser.Parser.parse(Unknown Source)
	at javax.swing.text.html.parser.DocumentParser.parse(Unknown Source)
	at javax.swing.text.html.parser.ParserDelegator.parse(Unknown Source)
	at ir.vsr.HTMLFileParserThread.run(HTMLFileDocument.java:57)
java.lang.ArrayIndexOutOfBoundsException: 88
	at javax.swing.text.html.parser.ContentModel.first(Unknown Source)
	at javax.swing.text.html.parser.ContentModel.first(Unknown Source)
	at javax.swing.text.html.parser.ContentModelState.advance(Unknown Source)
	at javax.swing.text.html.parser.TagStack.advance(Unknown Source)
	at javax.swing.text.html.parser.Parser.legalElementContext(Unknown Source)
	at javax.swing.text.html.parser.Parser.legalElementContext(Unknown Source)
	at javax.swing.text.html.parser.Parser.legalElementContext(Unknown Source)
	at javax.swing.text.html.parser.Parser.legalTagContext(Unknown Source)
	at javax.swing.text.html.parser.Parser.parseTag(Unknown Source)
	at javax.swing.text.html.parser.Parser.parseContent(Unknown Source)
	at javax.swing.text.html.parser.Parser.parse(Unknown Source)
	at javax.swing.text.html.parser.DocumentParser.parse(Unknown Source)
	at javax.swing.text.html.parser.ParserDelegator.parse(Unknown Source)
	at ir.vsr.HTMLFileParserThread.run(HTMLFileDocument.java:57)
Created 900 examples.
Initializing k-Nearest Neighbor classifier...
Generating 10 fold CV learning curves...
Train Percentage: 0.0%
  Calculating results for fold 0
    Number of training examples: 0
    Train Accuracy = 100.0%; Test Accuracy = 28.889%
  Calculating results for fold 1
    Number of training examples: 0
    Train Accuracy = 100.0%; Test Accuracy = 41.111%
  Calculating results for fold 2
    Number of training examples: 0
    Train Accuracy = 100.0%; Test Accuracy = 34.444%
  Calculating results for fold 3
    Number of training examples: 0
    Train Accuracy = 100.0%; Test Accuracy = 37.778%
  Calculating results for fold 4
    Number of training examples: 0
    Train Accuracy = 100.0%; Test Accuracy = 26.667%
  Calculating results for fold 5
    Number of training examples: 0
    Train Accuracy = 100.0%; Test Accuracy = 26.667%
  Calculating results for fold 6
    Number of training examples: 0
    Train Accuracy = 100.0%; Test Accuracy = 24.444%
  Calculating results for fold 7
    Number of training examples: 0
    Train Accuracy = 100.0%; Test Accuracy = 32.222%
  Calculating results for fold 8
    Number of training examples: 0
    Train Accuracy = 100.0%; Test Accuracy = 21.111%
  Calculating results for fold 9
    Number of training examples: 0
    Train Accuracy = 100.0%; Test Accuracy = 31.111%
Train Percentage: 1.0%
  Calculating results for fold 0
    Number of training examples: 9
    Train Accuracy = 100.0%; Test Accuracy = 40.0%
  Calculating results for fold 1
    Number of training examples: 9
    Train Accuracy = 100.0%; Test Accuracy = 40.0%
  Calculating results for fold 2
    Number of training examples: 9
    Train Accuracy = 100.0%; Test Accuracy = 35.556%
  Calculating results for fold 3
    Number of training examples: 9
    Train Accuracy = 100.0%; Test Accuracy = 48.889%
  Calculating results for fold 4
    Number of training examples: 9
    Train Accuracy = 100.0%; Test Accuracy = 37.778%
  Calculating results for fold 5
    Number of training examples: 9
    Train Accuracy = 100.0%; Test Accuracy = 36.667%
  Calculating results for fold 6
    Number of training examples: 9
    Train Accuracy = 100.0%; Test Accuracy = 36.667%
  Calculating results for fold 7
    Number of training examples: 9
    Train Accuracy = 100.0%; Test Accuracy = 33.333%
  Calculating results for fold 8
    Number of training examples: 9
    Train Accuracy = 100.0%; Test Accuracy = 46.667%
  Calculating results for fold 9
    Number of training examples: 9
    Train Accuracy = 100.0%; Test Accuracy = 27.778%
Train Percentage: 5.0%
  Calculating results for fold 0
    Number of training examples: 42
    Train Accuracy = 97.619%; Test Accuracy = 50.0%
  Calculating results for fold 1
    Number of training examples: 42
    Train Accuracy = 95.238%; Test Accuracy = 53.333%
  Calculating results for fold 2
    Number of training examples: 42
    Train Accuracy = 97.619%; Test Accuracy = 44.444%
  Calculating results for fold 3
    Number of training examples: 42
    Train Accuracy = 95.238%; Test Accuracy = 48.889%
  Calculating results for fold 4
    Number of training examples: 42
    Train Accuracy = 97.619%; Test Accuracy = 43.333%
  Calculating results for fold 5
    Number of training examples: 42
    Train Accuracy = 97.619%; Test Accuracy = 43.333%
  Calculating results for fold 6
    Number of training examples: 42
    Train Accuracy = 95.238%; Test Accuracy = 48.889%
  Calculating results for fold 7
    Number of training examples: 42
    Train Accuracy = 95.238%; Test Accuracy = 48.889%
  Calculating results for fold 8
    Number of training examples: 42
    Train Accuracy = 97.619%; Test Accuracy = 43.333%
  Calculating results for fold 9
    Number of training examples: 42
    Train Accuracy = 97.619%; Test Accuracy = 43.333%
Train Percentage: 10.0%
  Calculating results for fold 0
    Number of training examples: 81
    Train Accuracy = 98.765%; Test Accuracy = 62.222%
  Calculating results for fold 1
    Number of training examples: 81
    Train Accuracy = 96.296%; Test Accuracy = 57.778%
  Calculating results for fold 2
    Number of training examples: 81
    Train Accuracy = 96.296%; Test Accuracy = 52.222%
  Calculating results for fold 3
    Number of training examples: 81
    Train Accuracy = 96.296%; Test Accuracy = 57.778%
  Calculating results for fold 4
    Number of training examples: 81
    Train Accuracy = 96.296%; Test Accuracy = 55.556%
  Calculating results for fold 5
    Number of training examples: 81
    Train Accuracy = 96.296%; Test Accuracy = 53.333%
  Calculating results for fold 6
    Number of training examples: 81
    Train Accuracy = 97.531%; Test Accuracy = 55.556%
  Calculating results for fold 7
    Number of training examples: 81
    Train Accuracy = 97.531%; Test Accuracy = 50.0%
  Calculating results for fold 8
    Number of training examples: 81
    Train Accuracy = 96.296%; Test Accuracy = 55.556%
  Calculating results for fold 9
    Number of training examples: 81
    Train Accuracy = 96.296%; Test Accuracy = 45.556%
Train Percentage: 20.0%
  Calculating results for fold 0
    Number of training examples: 162
    Train Accuracy = 98.765%; Test Accuracy = 57.778%
  Calculating results for fold 1
    Number of training examples: 162
    Train Accuracy = 98.148%; Test Accuracy = 57.778%
  Calculating results for fold 2
    Number of training examples: 162
    Train Accuracy = 98.148%; Test Accuracy = 47.778%
  Calculating results for fold 3
    Number of training examples: 162
    Train Accuracy = 97.531%; Test Accuracy = 60.0%
  Calculating results for fold 4
    Number of training examples: 162
    Train Accuracy = 97.531%; Test Accuracy = 56.667%
  Calculating results for fold 5
    Number of training examples: 162
    Train Accuracy = 98.148%; Test Accuracy = 53.333%
  Calculating results for fold 6
    Number of training examples: 162
    Train Accuracy = 98.148%; Test Accuracy = 56.667%
  Calculating results for fold 7
    Number of training examples: 162
    Train Accuracy = 97.531%; Test Accuracy = 62.222%
  Calculating results for fold 8
    Number of training examples: 162
    Train Accuracy = 98.148%; Test Accuracy = 60.0%
  Calculating results for fold 9
    Number of training examples: 162
    Train Accuracy = 97.531%; Test Accuracy = 46.667%
Train Percentage: 30.0%
  Calculating results for fold 0
    Number of training examples: 243
    Train Accuracy = 97.531%; Test Accuracy = 55.556%
  Calculating results for fold 1
    Number of training examples: 243
    Train Accuracy = 97.531%; Test Accuracy = 66.667%
  Calculating results for fold 2
    Number of training examples: 243
    Train Accuracy = 96.708%; Test Accuracy = 46.667%
  Calculating results for fold 3
    Number of training examples: 243
    Train Accuracy = 97.942%; Test Accuracy = 62.222%
  Calculating results for fold 4
    Number of training examples: 243
    Train Accuracy = 97.942%; Test Accuracy = 57.778%
  Calculating results for fold 5
    Number of training examples: 243
    Train Accuracy = 97.531%; Test Accuracy = 55.556%
  Calculating results for fold 6
    Number of training examples: 243
    Train Accuracy = 97.942%; Test Accuracy = 54.444%
  Calculating results for fold 7
    Number of training examples: 243
    Train Accuracy = 97.531%; Test Accuracy = 60.0%
  Calculating results for fold 8
    Number of training examples: 243
    Train Accuracy = 97.942%; Test Accuracy = 64.444%
  Calculating results for fold 9
    Number of training examples: 243
    Train Accuracy = 97.531%; Test Accuracy = 53.333%
Train Percentage: 40.0%
  Calculating results for fold 0
    Number of training examples: 324
    Train Accuracy = 97.531%; Test Accuracy = 63.333%
  Calculating results for fold 1
    Number of training examples: 324
    Train Accuracy = 96.605%; Test Accuracy = 62.222%
  Calculating results for fold 2
    Number of training examples: 324
    Train Accuracy = 96.914%; Test Accuracy = 53.333%
  Calculating results for fold 3
    Number of training examples: 324
    Train Accuracy = 97.84%; Test Accuracy = 57.778%
  Calculating results for fold 4
    Number of training examples: 324
    Train Accuracy = 97.531%; Test Accuracy = 61.111%
  Calculating results for fold 5
    Number of training examples: 324
    Train Accuracy = 97.222%; Test Accuracy = 51.111%
  Calculating results for fold 6
    Number of training examples: 324
    Train Accuracy = 97.222%; Test Accuracy = 54.444%
  Calculating results for fold 7
    Number of training examples: 324
    Train Accuracy = 96.914%; Test Accuracy = 58.889%
  Calculating results for fold 8
    Number of training examples: 324
    Train Accuracy = 97.531%; Test Accuracy = 60.0%
  Calculating results for fold 9
    Number of training examples: 324
    Train Accuracy = 97.222%; Test Accuracy = 52.222%
Train Percentage: 50.0%
  Calculating results for fold 0
    Number of training examples: 405
    Train Accuracy = 97.284%; Test Accuracy = 65.556%
  Calculating results for fold 1
    Number of training examples: 405
    Train Accuracy = 96.543%; Test Accuracy = 60.0%
  Calculating results for fold 2
    Number of training examples: 405
    Train Accuracy = 97.531%; Test Accuracy = 52.222%
  Calculating results for fold 3
    Number of training examples: 405
    Train Accuracy = 97.778%; Test Accuracy = 56.667%
  Calculating results for fold 4
    Number of training examples: 405
    Train Accuracy = 97.284%; Test Accuracy = 55.556%
  Calculating results for fold 5
    Number of training examples: 405
    Train Accuracy = 97.284%; Test Accuracy = 54.444%
  Calculating results for fold 6
    Number of training examples: 405
    Train Accuracy = 97.037%; Test Accuracy = 60.0%
  Calculating results for fold 7
    Number of training examples: 405
    Train Accuracy = 97.037%; Test Accuracy = 60.0%
  Calculating results for fold 8
    Number of training examples: 405
    Train Accuracy = 97.037%; Test Accuracy = 60.0%
  Calculating results for fold 9
    Number of training examples: 405
    Train Accuracy = 97.284%; Test Accuracy = 56.667%
Train Percentage: 60.0%
  Calculating results for fold 0
    Number of training examples: 486
    Train Accuracy = 96.708%; Test Accuracy = 67.778%
  Calculating results for fold 1
    Number of training examples: 486
    Train Accuracy = 97.325%; Test Accuracy = 62.222%
  Calculating results for fold 2
    Number of training examples: 486
    Train Accuracy = 96.296%; Test Accuracy = 55.556%
  Calculating results for fold 3
    Number of training examples: 486
    Train Accuracy = 96.708%; Test Accuracy = 60.0%
  Calculating results for fold 4
    Number of training examples: 486
    Train Accuracy = 96.914%; Test Accuracy = 53.333%
  Calculating results for fold 5
    Number of training examples: 486
    Train Accuracy = 96.296%; Test Accuracy = 56.667%
  Calculating results for fold 6
    Number of training examples: 486
    Train Accuracy = 96.091%; Test Accuracy = 53.333%
  Calculating results for fold 7
    Number of training examples: 486
    Train Accuracy = 96.914%; Test Accuracy = 55.556%
  Calculating results for fold 8
    Number of training examples: 486
    Train Accuracy = 96.296%; Test Accuracy = 57.778%
  Calculating results for fold 9
    Number of training examples: 486
    Train Accuracy = 96.502%; Test Accuracy = 54.444%
Train Percentage: 70.0%
  Calculating results for fold 0
    Number of training examples: 567
    Train Accuracy = 96.473%; Test Accuracy = 66.667%
  Calculating results for fold 1
    Number of training examples: 567
    Train Accuracy = 96.473%; Test Accuracy = 66.667%
  Calculating results for fold 2
    Number of training examples: 567
    Train Accuracy = 96.473%; Test Accuracy = 52.222%
  Calculating results for fold 3
    Number of training examples: 567
    Train Accuracy = 97.178%; Test Accuracy = 60.0%
  Calculating results for fold 4
    Number of training examples: 567
    Train Accuracy = 96.473%; Test Accuracy = 55.556%
  Calculating results for fold 5
    Number of training examples: 567
    Train Accuracy = 95.944%; Test Accuracy = 54.444%
  Calculating results for fold 6
    Number of training examples: 567
    Train Accuracy = 96.825%; Test Accuracy = 54.444%
  Calculating results for fold 7
    Number of training examples: 567
    Train Accuracy = 95.944%; Test Accuracy = 54.444%
  Calculating results for fold 8
    Number of training examples: 567
    Train Accuracy = 96.649%; Test Accuracy = 57.778%
  Calculating results for fold 9
    Number of training examples: 567
    Train Accuracy = 96.12%; Test Accuracy = 56.667%
Train Percentage: 80.0%
  Calculating results for fold 0
    Number of training examples: 648
    Train Accuracy = 96.451%; Test Accuracy = 67.778%
  Calculating results for fold 1
    Number of training examples: 648
    Train Accuracy = 95.833%; Test Accuracy = 65.556%
  Calculating results for fold 2
    Number of training examples: 648
    Train Accuracy = 96.142%; Test Accuracy = 51.111%
  Calculating results for fold 3
    Number of training examples: 648
    Train Accuracy = 96.296%; Test Accuracy = 58.889%
  Calculating results for fold 4
    Number of training examples: 648
    Train Accuracy = 96.605%; Test Accuracy = 53.333%
  Calculating results for fold 5
    Number of training examples: 648
    Train Accuracy = 95.833%; Test Accuracy = 57.778%
  Calculating results for fold 6
    Number of training examples: 648
    Train Accuracy = 96.605%; Test Accuracy = 55.556%
  Calculating results for fold 7
    Number of training examples: 648
    Train Accuracy = 96.142%; Test Accuracy = 53.333%
  Calculating results for fold 8
    Number of training examples: 648
    Train Accuracy = 96.451%; Test Accuracy = 57.778%
  Calculating results for fold 9
    Number of training examples: 648
    Train Accuracy = 96.451%; Test Accuracy = 58.889%
Train Percentage: 90.0%
  Calculating results for fold 0
    Number of training examples: 729
    Train Accuracy = 96.296%; Test Accuracy = 63.333%
  Calculating results for fold 1
    Number of training examples: 729
    Train Accuracy = 95.61%; Test Accuracy = 64.444%
  Calculating results for fold 2
    Number of training examples: 729
    Train Accuracy = 95.336%; Test Accuracy = 53.333%
  Calculating results for fold 3
    Number of training examples: 729
    Train Accuracy = 96.159%; Test Accuracy = 58.889%
  Calculating results for fold 4
    Number of training examples: 729
    Train Accuracy = 96.296%; Test Accuracy = 54.444%
  Calculating results for fold 5
    Number of training examples: 729
    Train Accuracy = 95.61%; Test Accuracy = 57.778%
  Calculating results for fold 6
    Number of training examples: 729
    Train Accuracy = 96.571%; Test Accuracy = 58.889%
  Calculating results for fold 7
    Number of training examples: 729
    Train Accuracy = 96.022%; Test Accuracy = 54.444%
  Calculating results for fold 8
    Number of training examples: 729
    Train Accuracy = 96.571%; Test Accuracy = 60.0%
  Calculating results for fold 9
    Number of training examples: 729
    Train Accuracy = 96.022%; Test Accuracy = 56.667%
Train Percentage: 100.0%
  Calculating results for fold 0
    Number of training examples: 810
    Train Accuracy = 95.926%; Test Accuracy = 64.444%
  Calculating results for fold 1
    Number of training examples: 810
    Train Accuracy = 95.802%; Test Accuracy = 61.111%
  Calculating results for fold 2
    Number of training examples: 810
    Train Accuracy = 95.556%; Test Accuracy = 57.778%
  Calculating results for fold 3
    Number of training examples: 810
    Train Accuracy = 96.173%; Test Accuracy = 58.889%
  Calculating results for fold 4
    Number of training examples: 810
    Train Accuracy = 96.049%; Test Accuracy = 57.778%
  Calculating results for fold 5
    Number of training examples: 810
    Train Accuracy = 95.802%; Test Accuracy = 54.444%
  Calculating results for fold 6
    Number of training examples: 810
    Train Accuracy = 96.42%; Test Accuracy = 54.444%
  Calculating results for fold 7
    Number of training examples: 810
    Train Accuracy = 95.679%; Test Accuracy = 55.556%
  Calculating results for fold 8
    Number of training examples: 810
    Train Accuracy = 95.432%; Test Accuracy = 61.111%
  Calculating results for fold 9
    Number of training examples: 810
    Train Accuracy = 95.432%; Test Accuracy = 62.222%

Total Training time in seconds: 3.835
Testing time per example in milliseconds: 0.33
GNUPLOT train accuracy file is K-NearestNeighborTrain.gplot
GNUPLOT test accuracy file is K-NearestNeighbor.gplot
